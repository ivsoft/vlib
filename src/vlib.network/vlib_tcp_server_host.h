/*
Copyright (c) 2022, Vadim Malyshev, lboss75@gmail.com
All rights reserved
*/
#pragma once

#include "vlib_service_provider.h"
#include "vlib_moveonly_function.h"
#include "vlib_tcp_server.h"
#include "vlib_connection.h"
#include "vlib_host.h"

namespace vlib {
  namespace network {
    class tcp_server_host {
    public:
      static vlib::core::service_collection& bind(
        vlib::core::service_collection& services,
        const network_address& address);

      static void when(
        const std::shared_ptr<vlib::core::service_provider>& service_provider,
        vlib::core::moveonly_function<bool(connection&)> filter,
        vlib::core::moveonly_function<void(const std::shared_ptr<vlib::core::service_provider>&)> body);


    };

    class tcp_server_host_manager {
    public:
      static void add_server_host_manager(vlib::core::service_collection& services);

      using connection_handler_t = core::moveonly_function_noexcept<core::task<bool> (connection& connection, vlib::core::cancellation_token token)>;

      connection_pool& pool() { return _pool; }

      core::task<void> new_connection(connection&, vlib::core::cancellation_token) noexcept;
      void add_handler(connection_handler_t handler) {
        _handlers.emplace_back(std::move(handler));
      }
    private:
      connection_pool _pool;
      std::list<connection_handler_t> _handlers;
    };

    class tcp_server_hosted_service : public core::hosted_service, private tcp_server_client {
    public:
      tcp_server_hosted_service(std::shared_ptr<tcp_server_host_manager> host_manager, network_address address);

      core::task<void> start(core::cancellation_token token) override;
      core::task<void> stop(core::cancellation_token token) override;

      core::task<void> new_connection(connection&, vlib::core::cancellation_token) noexcept override;

    private:
      std::shared_ptr<tcp_server_host_manager> _host_manager;
      network_address _address;
      tcp_server _server;
    };
  }
}