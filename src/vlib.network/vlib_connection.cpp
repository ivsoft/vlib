#include <cstring>
#include "vlib_connection.h"
#include "vlib_stream.h"
#include "vlib_cancelled_error.h"

#ifndef _WIN32
#include <sys/epoll.h>
#include "vlib_connection_pool.h"
#endif


#ifdef _WIN32
vlib::network::connection::connection(SOCKET s, network_address partner) noexcept
	: _s(s), _partner(std::move(partner)), _input_stream(s), _output_stream(s)
{
}

vlib::network::connection::network_input_stream::network_input_stream(SOCKET handle)
 : _handle(handle)
{	
}

vlib::core::task<size_t> vlib::network::connection::network_input_stream::read(void* buffer, size_t size, core::cancellation_token context)
{
	std::lock_guard<std::mutex> lock(_current_mutex);

	auto current = std::make_unique<vlib::core::task<size_t>::promise_type>();
	if (_current) {
		current->set_exception(std::make_exception_ptr(std::runtime_error("Invalid operation: double network read")));
	}
	_current = std::move(current);

	memset(&_overlapped, 0, sizeof(_overlapped));
	_wsa_buf.len = (ULONG)size;
	_wsa_buf.buf = (CHAR*)buffer;

	_cancellation_handler = context.on_cancel([this]() {
		CancelIoEx((HANDLE)_handle, &_overlapped);
		return core::task<void>::completed();
	});

	DWORD flags = 0;
	DWORD numberOfBytesRecvd;
	if (NOERROR != WSARecv(
		_handle,
		&_wsa_buf,
		1,
		&numberOfBytesRecvd,
		&flags,
		&_overlapped,
		NULL)) {
		auto errorCode = WSAGetLastError();
		if (WSA_IO_PENDING != errorCode) {
			current = std::move(_current);
			if (WSAESHUTDOWN == errorCode || WSAECONNABORTED == errorCode) {
				current->return_value(0);
			}
			else {
				current->set_exception(std::make_exception_ptr(std::system_error(errorCode, std::system_category(), "read from tcp socket")));
			}
			return current->get_return_object();
		}
	}

	return _current->get_return_object();
}

void vlib::network::connection::network_input_stream::process(DWORD dwBytesTransfered) noexcept
{
	std::unique_lock<std::mutex> lock(_current_mutex);
	_cancellation_handler.remove();
	std::unique_ptr<vlib::core::task<size_t>::promise_type> current = std::move(_current);
	lock.unlock();

	current->return_value(dwBytesTransfered);
}

void vlib::network::connection::network_input_stream::error(DWORD error_code) noexcept
{
	std::unique_lock<std::mutex> lock(_current_mutex);
	_cancellation_handler.remove();
	std::unique_ptr<vlib::core::task<size_t>::promise_type> current = std::move(_current);
	lock.unlock();

	switch (error_code) {
	case ERROR_OPERATION_ABORTED:
		current->set_exception(std::make_exception_ptr(core::cancelled_error("tcp socket read operation was cancelled")));
		break;
	default:
		_current->set_exception(std::make_exception_ptr(std::system_error(error_code, std::system_category(), "read from tcp socket")));
		break;
	}
	current.reset();
}

vlib::network::connection::network_output_stream::network_output_stream(SOCKET handle)
	: _handle(handle)
{
}

vlib::core::task<size_t> vlib::network::connection::network_output_stream::write(const void* buffer, size_t size, core::cancellation_token token)
{
	std::lock_guard<std::mutex> lock(_current_mutex);

	auto current = std::make_unique<vlib::core::task<size_t>::promise_type>();
	if (_current) {
		current->set_exception(std::make_exception_ptr(std::runtime_error("Invalid operation: double network read")));
		return current->get_return_object();

	}
	_current = std::move(current);
	_cancellation_handler = token.on_cancel([this]() {
		CancelIoEx((HANDLE)_handle, &_overlapped);
		return core::task<void>::completed();
	});

	memset(&_overlapped, 0, sizeof(_overlapped));
	_wsa_buf.len = (ULONG)size;
	_wsa_buf.buf = (CHAR*)buffer;

	DWORD numberOfBytesRecvd;
	if (NOERROR != WSASend(
		_handle,
		&_wsa_buf,
		1,
		&numberOfBytesRecvd,
		0,
		&_overlapped,
		NULL)) {
		auto errorCode = WSAGetLastError();
		if (WSA_IO_PENDING != errorCode) {
			_cancellation_handler.remove();
			current = std::move(_current);
			current->set_exception(std::make_exception_ptr(std::system_error(errorCode, std::system_category(), "read from tcp socket")));
			return current->get_return_object();
		}
	}

	return _current->get_return_object();
}

vlib::core::task<void> vlib::network::connection::network_output_stream::close()
{
	shutdown(_handle, SD_SEND);
	co_return;
}

void vlib::network::connection::network_output_stream::process(DWORD dwBytesTransfered) noexcept
{
	std::unique_lock<std::mutex> lock(_current_mutex);

	_cancellation_handler.remove();
	std::unique_ptr<vlib::core::task<size_t>::promise_type> current = std::move(_current);
	lock.unlock();

	current->return_value(dwBytesTransfered);
}

void vlib::network::connection::network_output_stream::error(DWORD error_code) noexcept
{
	std::unique_lock<std::mutex> lock(_current_mutex);
	_cancellation_handler.remove();
	std::unique_ptr<vlib::core::task<size_t>::promise_type> current = std::move(_current);
	lock.unlock();

	switch (error_code) {
	case ERROR_OPERATION_ABORTED:
		current->set_exception(std::make_exception_ptr(core::cancelled_error("tcp socket write operation was cancelled")));
		break;

	default:
		current->set_exception(std::make_exception_ptr(std::system_error(error_code, std::system_category(), "write tcp socket")));
		break;
	}
}

vlib::network::connection::~connection() noexcept
{
	if (INVALID_SOCKET != _s) {
		closesocket(_s);
		_s = INVALID_SOCKET;
	}
}

#else// _WIN32

vlib::network::connection::connection(SOCKET s, network_address partner, connection_pool * pool) noexcept
	: _s(s), _partner(std::move(partner)), _input_stream(s, this), _output_stream(s, this), _pool(pool), _event_mask(0)
{
}

vlib::network::connection::~connection() noexcept
{
	if(0 < _s){
		::close(_s);
		_s = -1;
	}
}

vlib::network::connection::network_input_stream::network_input_stream(SOCKET handle, connection * owner)
 : _handle(handle), _owner(owner), _is_closed(false)
{
}

void vlib::network::connection::process(uint32_t events) noexcept
{
  if(EPOLLOUT == (EPOLLOUT & events)){
    _output_stream.process();
  }

  if(EPOLLIN == (EPOLLIN & events)){
    _input_stream.process();
  }

  if(0 != (events & ~(EPOLLIN | EPOLLOUT))){
    _input_stream.close_read();
  }
}
void vlib::network::connection::change_mask(
	uint32_t set_events,
	uint32_t clear_events) noexcept
{
	std::lock_guard<std::mutex> lock(_event_mask_mutex);

	auto last_mask = _event_mask;
	_event_mask |= set_events;
	_event_mask &= ~clear_events;
	if(last_mask != _event_mask){
		if(0 == last_mask){
			_pool->add_mask(_s, _event_mask);
		}
		else if (0 == _event_mask){
			_pool->delete_mask(_s);
		}
		else {
			_pool->change_mask(_s, _event_mask);
		}
	}
}

vlib::core::task<size_t> vlib::network::connection::network_input_stream::read(void* buffer, size_t size, core::cancellation_token token)
{
	std::lock_guard<std::mutex> lock(_current_mutex);
	auto current = std::make_unique<vlib::core::task<size_t>::promise_type>();
	if (_current) {
		current->set_exception(std::make_exception_ptr(std::runtime_error("Invalid operation: double network read")));
		return current->get_return_object();
	}
	_current = std::move(current);
	_buffer = buffer;
	_size = size;

    const auto len = _is_closed ? 0 : ::read(_handle, buffer, size);

	if (len < 0) {
		int error = errno;
		if (EAGAIN == error) {
			_cancellation_handler = token.on_cancel([this]() noexcept -> core::task<void> {
				std::unique_lock<std::mutex> lock(_current_mutex);
				auto current = std::move(_current);
				lock.unlock();

				if(current) {
					current->set_exception(std::make_exception_ptr(core::cancelled_error("socket read operation was cancelled")));
				}
				return core::task<void>::completed();
			});
			_owner->change_mask(EPOLLIN);
			return _current->get_return_object();
		}
		else {
			_owner->change_mask(0, EPOLLIN);
			current = std::move(_current);
			current->set_exception(std::make_exception_ptr(std::system_error(error, std::generic_category(), "read socket")));
			return current->get_return_object();
		}
	}
	else {
		current = std::move(_current);
		current->return_value(len);
		return current->get_return_object();
	}
}

void vlib::network::connection::network_input_stream::process() noexcept
{
	std::unique_lock<std::mutex> lock(_current_mutex);

	_owner->change_mask(0, EPOLLIN);
	const auto len = ::read(_handle, _buffer, _size);

	if (len < 0) {
		int error = errno;
		if (EAGAIN == error) {
			_owner->change_mask(EPOLLIN);
		}
		else {
			auto current = std::move(_current);
			_cancellation_handler.remove();
			lock.unlock();
			if(current) {
				current->set_exception(std::make_exception_ptr(std::system_error(error, std::generic_category(), "read socket")));
			}
		}
	}
	else if(!_current) {//
		if (0 != len) {
			printf("Invalid vlib::network::connection::network_input_stream::process\n");
		}

		shutdown(_handle, SHUT_RD);
		_is_closed = true;
	}
	else {
		auto current = std::move(_current);
		_cancellation_handler.remove();
		lock.unlock();
		current->return_value(len);
	}
}

void vlib::network::connection::network_input_stream::close_read() noexcept
{
	shutdown(_handle, SHUT_RD);

	std::unique_lock<std::mutex> lock(_current_mutex);
	if (_current) {
		auto current = std::move(_current);
		_cancellation_handler.remove();
		lock.unlock();
		current->return_value(0);
	} else {
		_is_closed = true;
	}
}

vlib::network::connection::network_output_stream::network_output_stream(SOCKET handle, connection * owner)
 : _handle(handle), _owner(owner), _is_closed(false)
{	
}

vlib::core::task<size_t> vlib::network::connection::network_output_stream::write(const void* buffer, size_t size, core::cancellation_token token)
{
	std::lock_guard<std::mutex> lock(_current_mutex);

	auto current = std::make_unique<vlib::core::task<size_t>::promise_type>();
	if (_current) {
		current->set_exception(std::make_exception_ptr(std::runtime_error("Invalid operation: double network write")));
		return current->get_return_object();
	}
	_current = std::move(current);
	_buffer = buffer;
	_size = size;

	int len = send(_handle, buffer, size, MSG_NOSIGNAL);
	if (len < 0) {
    	int error = errno;
        if (EAGAIN == error) {
            _owner->change_mask(EPOLLOUT);
			_cancellation_handler = token.on_cancel([this]() noexcept -> core::task<void> {
				std::unique_lock<std::mutex> lock(_current_mutex);
				auto current = std::move(_current);
				lock.unlock();

				if(current) {
					current->set_exception(std::make_exception_ptr(core::cancelled_error("socket write operation was cancelled")));
				}
				return core::task<void>::completed();
			});
    	} else {
				_current->set_exception(std::make_exception_ptr(std::system_error(error, std::generic_category(), "Send TCP")));
      }
	} else {
        _owner->change_mask(0, EPOLLOUT);
		current = std::move(_current);
		current->return_value(len);
		return current->get_return_object();
	}

	return _current->get_return_object();
}

vlib::core::task<void> vlib::network::connection::network_output_stream::close()
{
	std::lock_guard<std::mutex> lock(_current_mutex);
	if(0 >= _handle){
		shutdown(_handle, SHUT_WR);
	}
	_is_closed = true;
	co_return;
}

void vlib::network::connection::network_output_stream::process() noexcept
{
	std::unique_lock<std::mutex> lock(_current_mutex);
	
	_owner->change_mask(0, EPOLLOUT);
    const auto len = _is_closed ? 0 : ::write(_handle, _buffer, _size);

    if (len < 0) {
    	int error = errno;
        if (EAGAIN == error) {
        	_owner->change_mask(EPOLLOUT);
        }
		else {
			_cancellation_handler.remove();
			std::unique_ptr<vlib::core::task<size_t>::promise_type> current = std::move(_current);
			lock.unlock();
			if(current){
				current->set_exception(std::make_exception_ptr(std::system_error(error, std::generic_category(), "write socket")));
			}
		}
    }
    else {
		_cancellation_handler.remove();
		std::unique_ptr<vlib::core::task<size_t>::promise_type> current = std::move(_current);
		lock.unlock();
		current->return_value(len);
	}
}

#endif