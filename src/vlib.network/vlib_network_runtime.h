#pragma once

#ifdef _WIN32

#include <winsock2.h>
#include <Windows.h>
#include <WS2tcpip.h>
#include <iphlpapi.h>
#pragma comment(lib, "IPHLPAPI.lib")
#pragma comment(lib, "Ws2_32.lib")

#else

#include <sys/socket.h>
#include <sys/types.h>
#include <netdb.h>
#include <ifaddrs.h>
#include <net/if.h>

#endif


namespace vlib {
  namespace network {
    class runtime {
    public:
      runtime();
      ~runtime();


    };
  }
}