#include "vlib_tcp_server_host.h"

vlib::core::service_collection& vlib::network::tcp_server_host::bind(
  vlib::core::service_collection& services,
  const network_address& address) {
  tcp_server_host_manager::add_server_host_manager(services);

  services.add_singleton<core::hosted_service>([address](core::service_provider& sp) {
    return std::make_shared<tcp_server_hosted_service>(
      sp.get_required<tcp_server_host_manager>(),
      address);
  });

  return services;
}

void vlib::network::tcp_server_host_manager::add_server_host_manager(vlib::core::service_collection& services)
{
  if (!services.has<tcp_server_host_manager>()) {
    services.add_singleton<tcp_server_host_manager>([](core::service_provider& sp) {
      return std::make_shared<tcp_server_host_manager>();
      });
  }
}

vlib::core::task<void> vlib::network::tcp_server_host_manager::new_connection(connection& connection, vlib::core::cancellation_token token) noexcept
{
  for (const auto& handler : _handlers) {
    if (co_await handler(connection, token)) {
      co_return;
    }
  }
}

vlib::network::tcp_server_hosted_service::tcp_server_hosted_service(
  std::shared_ptr<tcp_server_host_manager> host_manager,
  network_address address)
  : _host_manager(host_manager), _address(address), _server(host_manager->pool()) {
}

vlib::core::task<void> vlib::network::tcp_server_hosted_service::start(
  core::cancellation_token token)
{
  _host_manager->pool().start(100);
  _server.start(_address, *this);

  return core::task<void>::completed();
}

vlib::core::task<void> vlib::network::tcp_server_hosted_service::stop(core::cancellation_token token)
{
  _server.stop();
  _host_manager->pool().stop();
  return core::task<void>::completed();
}

vlib::core::task<void> vlib::network::tcp_server_hosted_service::new_connection(connection& connection, vlib::core::cancellation_token token) noexcept
{
  return _host_manager->new_connection(connection, token);
}