#include "vlib_connection_pool.h"
#include <system_error>
#include <thread>

#ifdef _WIN32
#include <Windows.h>
#include <winsock2.h>
#include <iphlpapi.h>
#pragma comment(lib, "IPHLPAPI.lib")

#define NETWORK_EXIT 0xA1F8

#else

#include <sys/socket.h>
#include <sys/types.h>
#include <netdb.h>
#include <ifaddrs.h>
#include <net/if.h>
#include <sys/epoll.h>
#include <string.h>
#endif


vlib::network::connection_pool::connection_pool() : 
#ifdef _WIN32
    _handle(NULL)
#else
    _epoll_set(-1)
#endif
{
}

vlib::network::connection_pool::~connection_pool()
{
  stop();

#ifdef _WIN32
  if (NULL != _handle) {
    CloseHandle(_handle);
  }
#else
if(0 <= _epoll_set){
  ::close(_epoll_set);
}
#endif
}

void vlib::network::connection_pool::start(int max_connection)
{
#ifdef _WIN32
  _handle = CreateIoCompletionPort(INVALID_HANDLE_VALUE, NULL, 0, 0);

  if (NULL == _handle) {
    auto error = WSAGetLastError();
    throw std::system_error(error, std::system_category(), "Create I/O completion port");
  }

  //Create worker threads
  for (unsigned int i = 0; i < 2 * std::thread::hardware_concurrency(); ++i) {
    _work_threads.emplace_back(&connection_pool::thread_loop, this);
  }

#else
  _epoll_set = epoll_create(max_connection);
  if (0 > _epoll_set) {
    throw std::runtime_error("Out of memory for epoll_create");
  }
  _epoll_thread = std::thread(
    [this] {
      while(!_cancellation->is_cancelled()) {
        struct epoll_event events[1];

        auto result = epoll_wait(_epoll_set, events, sizeof(events) / sizeof(events[0]), 1000);
        if (0 > result) {
          auto error = errno;
          if (EINTR == error) {
            continue;
          }
          throw std::system_error(error, std::system_category(), "epoll_wait");
        }
        else if (0 < result) {
          for (int i = 0; i < result; ++i) {
            std::unique_lock<std::mutex> lock(_connections_mutex);
            auto p = _connections.find(events[i].data.fd);
            if (_connections.end() != p) {
              auto & handler = p->second;
              lock.unlock();

              handler.process(events[i].events);
              break;
            }
          }
        }
      }
    });
#endif
}

void vlib::network::connection_pool::stop()
{
  _cancellation->cancel();

#ifndef _WIN32
  if (_epoll_thread.joinable()) {
    _epoll_thread.join();
  }
#else
  for (size_t i = 0; i < _work_threads.size(); ++i) {
    PostQueuedCompletionStatus(_handle, 0, NETWORK_EXIT, NULL);
  }
  for (auto & p : _work_threads) {
    p.join();
  }
  if (NULL != _handle) {
    CloseHandle(_handle);
    _handle = NULL;
  }

  _work_threads.clear();
#endif

  for (;;) {
    std::unique_lock<std::mutex> lock(_connections_mutex);

    if (_connections.empty()) {
      break;
    }

    _connections_cv.wait(lock, [this]() { return _connections.empty(); });
  }
}


#ifdef _WIN32

vlib::network::connection& vlib::network::connection_pool::associate(SOCKET s, network_address partner)
{
  std::lock_guard<std::mutex> lock(_connections_mutex);

  if (NULL == CreateIoCompletionPort((HANDLE)s, _handle, NULL, 0)) {
    auto error = GetLastError();
    throw std::system_error(error, std::system_category(), "Associate with input/output completion port");
  }
  return _connections.emplace(std::piecewise_construct, std::forward_as_tuple(s), std::forward_as_tuple(s, std::move(partner))).first->second;
}

void vlib::network::connection_pool::remove(SOCKET s)
{
  std::lock_guard<std::mutex> lock(_connections_mutex);
  _connections.erase(s);
  if (_connections.empty()) {
    _connections_cv.notify_all();
  }
}


void vlib::network::connection_pool::thread_loop()
{
  while (!_cancellation->is_cancelled()) {
    DWORD dwBytesTransfered = 0;
    ULONG_PTR lpContext;
    OVERLAPPED* pOverlapped = NULL;

    if (!GetQueuedCompletionStatus(
      _handle,
      &dwBytesTransfered,
      &lpContext,
      &pOverlapped,
      INFINITE)) {
      auto errorCode = GetLastError();
      if (errorCode == WAIT_TIMEOUT) {
        continue;
      }

      if (pOverlapped != NULL) {
        connection::socket_task::from_overlapped(pOverlapped)->error(errorCode);
        continue;
      }
      else {
        return;
      }
    }

    if (lpContext == NETWORK_EXIT) {
      return;
    }

    connection::socket_task::from_overlapped(pOverlapped)->process(dwBytesTransfered);
  }
}

#else

vlib::network::connection& vlib::network::connection_pool::associate(SOCKET s, network_address partner)
{
  std::lock_guard<std::mutex> lock(_connections_mutex);
  return _connections.emplace(std::piecewise_construct, std::forward_as_tuple(s), std::forward_as_tuple(s, std::move(partner), this)).first->second;
}

void vlib::network::connection_pool::remove(SOCKET s)
{
  std::lock_guard<std::mutex> lock(_connections_mutex);
  _connections.erase(s);

  if (_connections.empty()) {
    _connections_cv.notify_all();
  }
}

void vlib::network::connection_pool::add_mask(int s, uint32_t mask) {
 struct epoll_event event_data;
 memset(&event_data, 0, sizeof(event_data));
 event_data.events = mask | EPOLLHUP | EPOLLERR;
 event_data.data.fd = s;

 std::lock_guard<std::mutex> lock(_epoll_mutex);
 if(0 > epoll_ctl(_epoll_set, EPOLL_CTL_ADD, s, &event_data)) {
  const auto error = errno;
  throw std::system_error(error, std::system_category(), "vlib::network::connection_pool::add_mask");
 }
}

void vlib::network::connection_pool::change_mask(int s, uint32_t mask)  {
 struct epoll_event event_data;
 memset(&event_data, 0, sizeof(event_data));
 event_data.events = mask | EPOLLHUP | EPOLLERR;
 event_data.data.fd = s;

 std::lock_guard<std::mutex> lock(_epoll_mutex);
 if(0 > epoll_ctl(_epoll_set, EPOLL_CTL_MOD, s, &event_data)){
  const auto error = errno;
  throw std::system_error(error, std::system_category(), "vlib::network::connection_pool::change_mask");
 }
}

void vlib::network::connection_pool::delete_mask(int s) {
 struct epoll_event event_data;
 memset(&event_data, 0, sizeof(event_data));
 event_data.data.fd = s;

 std::lock_guard<std::mutex> lock(_epoll_mutex);
 if(0 > epoll_ctl(_epoll_set, EPOLL_CTL_DEL, s, &event_data)){
  const auto error = errno;
  throw std::system_error(error, std::system_category(), "vlib::network::connection_pool::delete_mask");
 }
}


#endif