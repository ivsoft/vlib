#include "vlib_network_address.h"
#include <system_error>
#include <cstring>

vlib::network::network_address::network_address() noexcept
    : addr_size_(sizeof(this->addr_)) {
    memset((char*)&this->addr_, 0, sizeof(this->addr_));
}

vlib::network::network_address::network_address(
    sa_family_t af,
    int sock_type,
    int ai_flags,
    int ai_protocol,
    const std::string_view& server,
    uint16_t port) {
    memset((char*)&this->addr_, 0, sizeof(this->addr_));

    addrinfo hints;

    memset(&hints, 0, sizeof(hints));
    hints.ai_family = af;
    hints.ai_socktype = sock_type;//SOCK_DGRAM;
    hints.ai_protocol = ai_protocol;// IPPROTO_UDP;
    hints.ai_flags = ai_flags | AI_NUMERICHOST; // AI_NUMERICSERV | AI_ALL | AI_V4MAPPED

    addrinfo* buffer;
    auto status = getaddrinfo(server.data(), std::to_string(port).c_str(), &hints, &buffer);
    if (status) {
        hints.ai_flags = ai_flags;
        status = getaddrinfo(server.data(), std::to_string(port).c_str(), &hints, &buffer);
        if (status) {
            throw std::system_error(status, std::system_category(), "Parse address " + std::string(server));
        }
    }

    for (auto res = buffer; res != nullptr; res = res->ai_next) {
        if (res->ai_family == af) {
            switch (af) {
            case AF_INET: {
                this->addr_size_ = sizeof(sockaddr_in);
                memcpy(&this->addr_, res->ai_addr, this->addr_size_);
                freeaddrinfo(buffer);
                return;
            }
            case AF_INET6: {
                this->addr_size_ = sizeof(sockaddr_in6);
                memcpy(&this->addr_, res->ai_addr, this->addr_size_);
                freeaddrinfo(buffer);
                return;
            }
            }
        }
    }
    freeaddrinfo(buffer);

    throw std::runtime_error("Unable to resolve " + std::string(server));
}

vlib::network::network_address vlib::network::network_address::tcp_ip4(const std::string_view& server, uint16_t port)
{
    return network_address(AF_INET, SOCK_STREAM, AI_NUMERICSERV | AI_ALL | AI_V4MAPPED, 0, server, port);
}

vlib::network::network_address vlib::network::network_address::tcp_ip6(const std::string_view& server, uint16_t port)
{
    return network_address(AF_INET6, SOCK_STREAM, AI_NUMERICSERV | AI_ALL | AI_V4MAPPED, 0, server, port);
}

vlib::network::network_address vlib::network::network_address::udp_ip4(const std::string_view& server, uint16_t port)
{
    return network_address(AF_INET, SOCK_DGRAM, AI_NUMERICSERV | AI_ALL | AI_V4MAPPED, IPPROTO_UDP, server, port);
}

vlib::network::network_address vlib::network::network_address::udp_ip6(const std::string_view& server, uint16_t port)
{
    return network_address(AF_INET6, SOCK_DGRAM, AI_NUMERICSERV | AI_ALL | AI_V4MAPPED, IPPROTO_UDP, server, port);
}

sa_family_t vlib::network::network_address::family() const noexcept {
    return reinterpret_cast<const sockaddr*>(&this->addr_)->sa_family;
}

std::string vlib::network::network_address::server() const {
    char buffer[512];

    const auto err = getnameinfo(
        (struct sockaddr*)&this->addr_,
        this->addr_size_,
        buffer,
        sizeof(buffer),
        0,
        0,
        NI_NUMERICHOST);

    if (0 != err) {
        const auto error = errno;
        throw std::system_error(error, std::system_category(), "getnameinfo");
    }

    return buffer;
}

uint16_t vlib::network::network_address::port() const {
    auto addr = (sockaddr*)&this->addr_;
    switch (addr->sa_family) {
    case AF_INET: {
        return ntohs(reinterpret_cast<const sockaddr_in*>(&this->addr_)->sin_port);
    }
    case AF_INET6: {
        return ntohs(reinterpret_cast<const sockaddr_in6*>(&this->addr_)->sin6_port);
    }
    default: {
        throw std::runtime_error("Unexpected network address family");
    }
    }
}

vlib::network::network_address::network_address(
    struct sockaddr* ai_addr,
    size_t ai_addrlen) noexcept
    : addr_size_((socklen_t)ai_addrlen) {

    memcpy((char*)&this->addr_, ai_addr, ai_addrlen);

}

vlib::network::network_address::network_address(
    sa_family_t af,
    uint16_t port) {
    memset((char*)&this->addr_, 0, sizeof(this->addr_));

    switch (af) {
    case AF_INET: {
        auto addr = (sockaddr_in*)&this->addr_;
        addr->sin_family = af;
        addr->sin_port = htons(port);
        addr->sin_addr.s_addr = INADDR_ANY;

        this->addr_size_ = sizeof(sockaddr_in);
        break;
    }
    case AF_INET6: {
        auto addr = (sockaddr_in6*)&this->addr_;
        addr->sin6_family = af;
        addr->sin6_port = htons(port);
        addr->sin6_addr = IN6ADDR_ANY_INIT;

        this->addr_size_ = sizeof(sockaddr_in6);
        break;
    }
    default:
        throw std::runtime_error("Invalid network family");
    }
}

bool vlib::network::network_address::is_martian() const {
    auto addr = (const sockaddr*)&this->addr_;
    switch (addr->sa_family) {
    case AF_INET: {
        struct sockaddr_in* sin = (struct sockaddr_in*)&this->addr_;
        const unsigned char* address = (const unsigned char*)&sin->sin_addr;
        return sin->sin_port == 0 ||
            (address[0] == 0) ||
            (address[0] == 127) ||
            ((address[0] & 0xE0) == 0xE0);
    }
    case AF_INET6: {
        static const unsigned char zeroes[20] = { 0 };
        static const unsigned char v4prefix[16] = {
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0xFF, 0xFF, 0, 0, 0, 0
        };
        struct sockaddr_in6* sin6 = (struct sockaddr_in6*)&this->addr_;
        const unsigned char* address = (const unsigned char*)&sin6->sin6_addr;
        return sin6->sin6_port == 0 ||
            (address[0] == 0xFF) ||
            (address[0] == 0xFE && (address[1] & 0xC0) == 0x80) ||
            (memcmp(address, zeroes, 15) == 0 &&
                (address[15] == 0 || address[15] == 1)) ||
            (memcmp(address, v4prefix, 12) == 0);
    }
    default:
        throw std::runtime_error("Invalid network family");
    }

    return false;
}

int vlib::network::network_address::compare(const network_address& other) const {
    if (this->addr_.ss_family == other.addr_.ss_family) {
        switch (this->addr_.ss_family) {
        case AF_INET: {
            const auto result = reinterpret_cast<const sockaddr_in*>(&this->addr_)->sin_addr.s_addr -
                reinterpret_cast<const sockaddr_in*>(&other.addr_)->sin_addr.s_addr;
            if (0 != result) {
                return result;
            }

            return reinterpret_cast<const sockaddr_in*>(&this->addr_)->sin_port -
                reinterpret_cast<const sockaddr_in*>(&other.addr_)->sin_port;
        }
        case AF_INET6: {
            const auto l = reinterpret_cast<const sockaddr_in6*>(&this->addr_);
            const auto r = reinterpret_cast<const sockaddr_in6*>(&other.addr_);
            auto result = memcmp(
                &l->sin6_addr,
                &r->sin6_addr,
                sizeof(l->sin6_addr));
            if (0 != result) {
                return result;
            }

            result = ntohs(l->sin6_port) - ntohs(r->sin6_port);
            if (0 != result) {
                return result;
            }

            result = l->sin6_flowinfo - r->sin6_flowinfo;
            if (0 != result) {
                return result;
            }

            return l->sin6_scope_id - r->sin6_scope_id;
        }
        default: {
            throw std::runtime_error("Invalid network family");
        }
        }
    }
    else {
        if (this->addr_.ss_family == AF_INET && other.addr_.ss_family == AF_INET6) {
            const auto l = reinterpret_cast<const sockaddr_in*>(&this->addr_);
            const auto r = reinterpret_cast<const sockaddr_in6*>(&other.addr_);
            if (IN6_IS_ADDR_V4MAPPED(&r->sin6_addr)) {
#ifdef _WIN32
                const auto result = l->sin_addr.s_addr - r->sin6_addr.u.Word[3];
#else
                const auto result = l->sin_addr.s_addr - r->sin6_addr.s6_addr32[3];
#endif
                if (0 != result) {
                    return result;
                }

                return l->sin_port - r->sin6_port;
            }
        }
        else
            if (this->addr_.ss_family == AF_INET6 && other.addr_.ss_family == AF_INET) {
                const auto l = reinterpret_cast<const sockaddr_in6*>(&this->addr_);
                const auto r = reinterpret_cast<const sockaddr_in*>(&other.addr_);
                if (IN6_IS_ADDR_V4MAPPED(&l->sin6_addr)) {
#ifdef _WIN32
                    const auto result = l->sin6_addr.u.Word[3] - r->sin_addr.s_addr;
#else
                    const auto result = l->sin6_addr.s6_addr32[3] - r->sin_addr.s_addr;
#endif
                    if (0 != result) {
                        return result;
                    }

                    return l->sin6_port - r->sin_port;
                }
            }

        return this->addr_.ss_family - other.addr_.ss_family;
    }

    return -1;
}




