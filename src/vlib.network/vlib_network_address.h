/*
Copyright (c) 2017, Vadim Malyshev, lboss75@gmail.com
All rights reserved
*/

#pragma once

#include <string>
#include <string_view>

#ifndef _WIN32
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
using SOCKET = int;
#define INVALID_SOCKET (-1)
#else
#include <winsock2.h>
#include <Windows.h>
#include <WS2tcpip.h>
using sa_family_t = ADDRESS_FAMILY;
#endif

namespace vlib {
  namespace network {
    class network_address {
    public:
      network_address() noexcept;
      network_address(network_address&&) noexcept = default;
      network_address(const network_address&) noexcept = default;

      network_address& operator = (network_address&&) noexcept = default;
      network_address& operator = (const network_address&) noexcept = default;

      network_address(
        sa_family_t af,
        int sock_type,
        int ai_flags,
        int ai_protocol,
        const std::string_view& server,
        uint16_t port);

      network_address(
        struct sockaddr* ai_addr,
        size_t ai_addrlen) noexcept;

      network_address(
        sa_family_t af,
        uint16_t port);

      static network_address any_ip4(
        uint16_t port) {
        return network_address(AF_INET, port);
      }

      static network_address any_ip6(
        uint16_t port) {
        return network_address(AF_INET6, port);
      }

      static network_address tcp_ip4(
        const std::string_view& server,
        uint16_t port);

      static network_address tcp_ip6(
        const std::string_view& server,
        uint16_t port);

      static network_address udp_ip4(
        const std::string_view& server,
        uint16_t port);

      static network_address udp_ip6(
        const std::string_view& server,
        uint16_t port);


      operator const sockaddr* () const noexcept {
        return (const sockaddr*)&this->addr_;
      }

      operator sockaddr* () noexcept {
        return (sockaddr*)&this->addr_;
      }

      sa_family_t family() const noexcept;

      std::string server() const;
      uint16_t port() const;

      socklen_t * size_ptr() noexcept {
        return &this->addr_size_;
      }

      socklen_t size() const noexcept {
        return this->addr_size_;
      }

      bool is_martian() const;

      int compare(const network_address& other) const;

      bool operator == (const network_address& other) const {
        return this->compare(other) == 0;
      }

      bool operator < (const network_address& other) const {
        return this->compare(other) < 0;
      }

      bool operator > (const network_address& other) const {
        return this->compare(other) > 0;
      }

      explicit operator bool() const {
        return this->addr_.ss_family != 0;
      }
      bool operator ! () const {
        return this->addr_.ss_family == 0;
      }

    private:
      sockaddr_storage addr_;
      socklen_t addr_size_;
    };
  }
}
