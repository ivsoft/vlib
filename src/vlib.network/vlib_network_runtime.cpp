#include "vlib_network_runtime.h"
#include <exception>
#include <system_error>


vlib::network::runtime::runtime()
{
#ifdef _WIN32
  //Initialize Winsock
  WSADATA wsaData;
  if (NO_ERROR != WSAStartup(MAKEWORD(2, 2), &wsaData)) {
    auto error = WSAGetLastError();
    throw std::system_error(error, std::system_category(), "Initiates Winsock");
  }
#else

#endif
}

vlib::network::runtime::~runtime()
{
#ifdef _WIN32
  WSACleanup();
#else

#endif
}
