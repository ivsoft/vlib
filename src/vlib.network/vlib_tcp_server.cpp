#include "vlib_tcp_server.h"

#ifndef _WIN32
#include <sys/ioctl.h>
#include <fcntl.h>
#include <sys/epoll.h>
#include <cstring>
#endif

vlib::network::tcp_server::tcp_server(connection_pool& pool)
  : _s(INVALID_SOCKET), _pool(pool)
{
#ifdef _WIN32
  _shutdown_event = CreateEvent(NULL, TRUE, FALSE, NULL);
  if (NULL == _shutdown_event) {
    auto error = GetLastError();
    throw std::system_error(error, std::system_category(), "CreateEvent");
  }
#endif
}

vlib::network::tcp_server::~tcp_server() noexcept
{
#ifdef _WIN32
  if (INVALID_SOCKET != _s) {
    closesocket(_s);
  }
  if (NULL != _shutdown_event) {
    CloseHandle(_shutdown_event);
  }
#else
  if(0 < _s){
    ::close(_s);
    _s = INVALID_SOCKET;
  }
#endif
}

void vlib::network::tcp_server::start(
  const network_address& address,
  tcp_server_client& client) {

  auto promise = std::make_shared<vlib::core::task<void>::promise_type>();

#ifdef _WIN32
  _s = WSASocket(address.family(), SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED);

  if (INVALID_SOCKET == _s) {
    const auto error = WSAGetLastError();
    throw std::system_error(error, std::system_category(), "create socket");
  }

  if (SOCKET_ERROR == ::bind(_s, address, address.size())) {
    const auto error = WSAGetLastError();
    throw std::system_error(error, std::system_category(), "bind");
  }

  if (SOCKET_ERROR == ::listen(_s, SOMAXCONN)) {
    const auto error = WSAGetLastError();
    throw std::system_error(error, std::system_category(), "listen socket");
  }

  _accept_event.select(_s, FD_ACCEPT);

  _wait_accept_task = std::thread(
    [this, &client]() {

      HANDLE events[2] = {
        _shutdown_event,
        _accept_event.handle()
      };
      for (;;) {
        auto result = WSAWaitForMultipleEvents(2, events, FALSE, INFINITE, FALSE);
        if ((WAIT_OBJECT_0 + 1) != result) {
          break;
      }
      WSANETWORKEVENTS WSAEvents;
      WSAEnumNetworkEvents(
        _s,
        _accept_event.handle(),
        &WSAEvents);
      if ((WSAEvents.lNetworkEvents & FD_ACCEPT)
        && (0 == WSAEvents.iErrorCode[FD_ACCEPT_BIT])) {
        //Process it
        sockaddr_in client_address;
        int client_address_length = sizeof(client_address);

        auto socket = accept(_s, (sockaddr*)&client_address, &client_address_length);
        if (INVALID_SOCKET != socket) {
          client.new_connection(_pool.associate(socket, network_address((sockaddr*)&client_address, client_address_length)), core::cancellation_token(_cancellation))
            .then([this, socket]() noexcept { _pool.remove(socket); });
        }
        else {
          const auto error = WSAGetLastError();
          client.accept_error(std::system_error(error, std::system_category(), "accept socket")).then([]() noexcept {});
        }
      }
    }
  });
#else
  _s = socket(AF_INET, SOCK_STREAM, 0);
  if (_s < 0) {
    auto error = errno;
    throw std::system_error(error, std::generic_category(), "create socket");
  }

  /*************************************************************/
  /* Allow socket descriptor to be reuseable                   */
  /*************************************************************/
  int on = 1;
  if (0 > setsockopt(_s, SOL_SOCKET, SO_REUSEADDR, (char*)&on, sizeof(on))) {
    auto error = errno;
    throw std::system_error(error, std::generic_category(), "setsockopt");
  }

  /*************************************************************/
  /* Set socket to be nonblocking. All of the sockets for    */
  /* the incoming connections will also be nonblocking since  */
  /* they will inherit that state from the listening socket.   */
  /*************************************************************/
  if (0 > ioctl(_s, FIONBIO, (char*)&on)) {
    auto error = errno;
    throw std::system_error(error, std::generic_category());
  }

  //bind to address
  if (0 > ::bind(_s, address, address.size())) {
    auto error = errno;
    throw std::system_error(error, std::generic_category());
  }

  if (0 > ::listen(_s, SOMAXCONN)) {
    auto error = errno;
    throw  std::system_error(error, std::generic_category());
  }

  /* Set the socket to non-blocking, this is essential in event
  * based programming with libevent. */

  auto flags = fcntl(_s, F_GETFL);
  if (0 > flags) {
    auto error = errno;
    throw std::system_error(error, std::generic_category());
  }

  flags |= O_NONBLOCK;
  if (0 > fcntl(_s, F_SETFL, flags)) {
    auto error = errno;
    throw std::system_error(error, std::generic_category());
  }

  auto epollfd = epoll_create(1);
  if (0 > epollfd) {
    auto error = errno;
    throw std::system_error(error, std::generic_category(), "epoll_create failed");
  }

  struct epoll_event ev;
  memset(&ev, 0, sizeof(ev));
  ev.events = EPOLLIN;
  ev.data.fd = _s;
  if (0 > epoll_ctl(epollfd, EPOLL_CTL_ADD, _s, &ev)) {
    auto error = errno;
    throw std::system_error(error, std::generic_category(), "epoll_create failed");
  }

  _wait_accept_task = std::thread(
    [this, epollfd, &client]() {

    while (!_cancellation->is_cancelled()) {
      struct epoll_event ev;
      memset(&ev, 0, sizeof(ev));
      ev.events = EPOLLIN;
      ev.data.fd = _s;

      auto result = epoll_wait(epollfd, &ev, 1, 1000);
      if (result > 0) {
        sockaddr client_address;
        socklen_t client_address_length = sizeof(client_address);

        auto socket = accept(_s, &client_address, &client_address_length);
        if (0 < socket) {
          //set_timeouts
          int optval = 1;
          socklen_t optlen = sizeof(optval);
          if(0 > setsockopt(socket, SOL_SOCKET, SO_KEEPALIVE, &optval, optlen)) {
            ::close(socket);
            continue;
          }

          //make_socket_non_blocking
          auto flags = fcntl(socket, F_GETFL, 0);
          if (flags == -1) {
            ::close(socket);
            continue;
          }

          flags |= O_NONBLOCK;
          auto s = fcntl(socket, F_SETFL, flags);
          if (s == -1) {
            ::close(socket);
            continue;
          }

          // struct timeval t;    
          // t.tv_sec = 30;
          // t.tv_usec = 30;
          // if(0 > setsockopt(socket, SOL_SOCKET, SO_RCVTIMEO, (const void *)&t, sizeof(t))) {
          //   ::close(socket);
          //   continue;
          // }

          client.new_connection(_pool.associate(socket, network_address(&client_address, client_address_length)), core::cancellation_token(_cancellation))
            .then([this, socket](core::task<void> result) { _pool.remove(socket); });
        }
        else {
          auto error = errno;
          client.accept_error(std::system_error(error, std::generic_category(), "accept socket")).then([]() noexcept {});
        }
      }
    }
  });
#endif
}

void vlib::network::tcp_server::stop()
{
  _cancellation->cancel();

#ifdef _WIN32
  SetEvent(_shutdown_event);
#endif
  _wait_accept_task.join();

  _pool.stop();
}

#ifdef _WIN32
vlib::network::tcp_server::windows_wsa_event::windows_wsa_event()
{
  _handle = WSACreateEvent();
  if (WSA_INVALID_EVENT == _handle) {
    auto error = WSAGetLastError();
    throw std::system_error(error, std::system_category(), "WSACreateEvent");
  }
}

vlib::network::tcp_server::windows_wsa_event::~windows_wsa_event() noexcept
{
  if (WSA_INVALID_EVENT != _handle) {
    WSACloseEvent(_handle);
  }
}

void vlib::network::tcp_server::windows_wsa_event::select(SOCKET s, long lNetworkEvents)
{
  if (SOCKET_ERROR == WSAEventSelect(s, _handle, FD_ACCEPT)) {
    auto error = WSAGetLastError();
    throw std::system_error(error, std::system_category(), "WSAEventSelect");
  }
}
#endif//

vlib::network::tcp_server_client::~tcp_server_client()
{
}

vlib::core::task<void> vlib::network::tcp_server_client::accept_error(std::system_error error) noexcept
{
  return vlib::core::task<void>::completed();
}
