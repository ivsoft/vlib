/*
Copyright (c) 2022, Vadim Malyshev, lboss75@gmail.com
All rights reserved
*/

#pragma once

#include "vlib_task.h"
#include "vlib_stream.h"
#include "vlib_network_address.h"
#include "vlib_cancellation_token.h"

namespace vlib {
  namespace network {
	class connection_pool;
    class connection {
    public:
		connection(SOCKET s, network_address partner
#ifndef _WIN32
		, connection_pool * pool
#endif// _WIN32
		) noexcept;
    	~connection() noexcept;

		SOCKET socket() const noexcept { return _s; }
		const network_address& partner() const noexcept { return _partner; }

    	core::input_stream& input_stream() noexcept { return _input_stream; }
    	core::output_stream& output_stream() noexcept { return _output_stream; }
#ifdef _WIN32
      class socket_task {
      public:
        virtual void process(DWORD dwBytesTransfered) noexcept = 0;
        virtual void error(DWORD errorCode) noexcept = 0;

        static socket_task* from_overlapped(OVERLAPPED* pOverlapped) noexcept {
          return reinterpret_cast<socket_task *>((uint8_t*)pOverlapped - offsetof(socket_task, _overlapped));
        }

      protected:
        OVERLAPPED _overlapped;
        WSABUF _wsa_buf;

      };
#else
    	void process(uint32_t events) noexcept;
    	void change_mask(uint32_t set_events, uint32_t clear_events = 0) noexcept;
#endif

    private:
			class network_input_stream : public core::input_stream
#ifdef _WIN32
			, private socket_task 
#endif
			{
			public:
				network_input_stream(SOCKET handle
#ifndef _WIN32
				, connection * owner
#endif
				);

				core::task<size_t> read(void* buffer, size_t size, core::cancellation_token token) override;

			private:
				SOCKET _handle;
				std::unique_ptr<vlib::core::task<size_t>::promise_type> _current;
				core::cancellation_token::handler_ref _cancellation_handler;
				std::mutex _current_mutex;

#ifdef _WIN32
				void process(DWORD dwBytesTransfered) noexcept override;
				void error(DWORD error_code) noexcept override;
#else
				connection* _owner;
				void* _buffer;
				size_t _size;
				bool _is_closed;
			public:
				void process() noexcept;
				void close_read() noexcept;
#endif
			};
			class network_output_stream : public core::output_stream
#ifdef _WIN32
			, private socket_task 
#endif
			{
			public:
				network_output_stream(SOCKET handle
#ifndef _WIN32
				, connection * owner
#endif
				);

				core::task<size_t> write(const void* buffer, size_t size, core::cancellation_token token) override;
				core::task<void> close() override;
			private:
				SOCKET _handle;
				std::unique_ptr<vlib::core::task<size_t>::promise_type> _current;
				core::cancellation_token::handler_ref _cancellation_handler;
				std::mutex _current_mutex;

#ifdef _WIN32
				void process(DWORD dwBytesTransfered) noexcept override;
				void error(DWORD error_code) noexcept override;
#else
				bool _is_closed;
				connection* _owner;
				const void* _buffer;
				size_t _size;
			public:
				void process() noexcept;
#endif
			};

			SOCKET _s;
			network_address _partner;
      network_input_stream _input_stream;
      network_output_stream _output_stream;
	  #ifndef _WIN32
			connection_pool * _pool;
	      	uint32_t _event_mask;
		  	std::mutex _event_mask_mutex;
	  #endif
    };
  }
}