/*
Copyright (c) 2022, Vadim Malyshev, lboss75@gmail.com
All rights reserved
*/
#pragma once

#include "vlib_moveonly_function.h"
#include "vlib_task.h"
#include "vlib_connection_pool.h"
#include "vlib_cancellation_token_source.h"

namespace vlib {
  namespace network {
    class network_address;
    class connection;

    class tcp_server_client {
    public:
      virtual ~tcp_server_client();
      virtual core::task<void> accept_error(std::system_error error) noexcept;
      virtual core::task<void> new_connection(connection&, vlib::core::cancellation_token) noexcept = 0;
    };

    class tcp_server {
    public:
      tcp_server(connection_pool& pool);
      ~tcp_server() noexcept;

      void start(
        const network_address& address,
        tcp_server_client & client);

      void stop();

    private:
      SOCKET _s;
      connection_pool & _pool;
      std::thread _wait_accept_task;
      std::shared_ptr<vlib::core::cancellation_token_source> _cancellation = std::make_shared<vlib::core::cancellation_token_source>();
#ifdef _WIN32
      class windows_wsa_event
      {
      public:
        windows_wsa_event();

        ~windows_wsa_event() noexcept;

        void select(SOCKET s, long lNetworkEvents);

        WSAEVENT handle() const {
          return _handle;
        }

      private:
        WSAEVENT _handle;
      };

      windows_wsa_event _accept_event;

      HANDLE _shutdown_event;

#endif
    };
  }
}