#pragma once

#include "vlib_task.h"
#include "vlib_connection_pool.h"

namespace vlib {
  namespace network {
    class network_address;
    class connection;

    class tcp_client {
    public:
      tcp_client(connection_pool& pool);

      connection & connect(const network_address& address);
      void close(connection& c);

    private:
      connection_pool & _pool;
    };
  }
}