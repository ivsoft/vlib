/*
Copyright (c) 2022, Vadim Malyshev, lboss75@gmail.com
All rights reserved
*/

#pragma once

#include <map>
#include <list>
#include <thread>
#include <condition_variable>

#include "vlib_connection.h"
#include "vlib_cancellation_token_source.h"

namespace vlib {
  namespace network {
    class connection_pool {
    public:
      connection_pool();
      ~connection_pool();

      connection_pool(const connection_pool&) = delete;
      connection_pool(connection_pool&&) = delete;

      connection_pool& operator = (const connection_pool&) = delete;
      connection_pool& operator = (connection_pool&&) = delete;

      void start(int max_connection);
      void stop();

      connection& associate(SOCKET s, network_address partner);
      void remove(SOCKET s);
    private:
      std::map<SOCKET, connection> _connections;
      std::condition_variable _connections_cv;
      std::mutex _connections_mutex;
      std::shared_ptr<vlib::core::cancellation_token_source> _cancellation = std::make_shared<vlib::core::cancellation_token_source>();

#ifdef _WIN32
      HANDLE _handle;
      std::list<std::thread> _work_threads;

      void thread_loop();
#else
      friend class connection;
      int _epoll_set;
      std::mutex _epoll_mutex;
      std::thread _epoll_thread;

      void add_mask(int s, uint32_t mask);
      void change_mask(int s, uint32_t mask);
      void delete_mask(int s);
#endif//_WIN32

    };
  }
}