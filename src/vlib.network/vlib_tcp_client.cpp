#include <system_error>
#include "vlib_tcp_client.h"
#include <fcntl.h>

#ifndef _WIN32
#include <unistd.h>
#endif

vlib::network::tcp_client::tcp_client(connection_pool& pool)
  : _pool(pool)
{
}

vlib::network::connection & vlib::network::tcp_client::connect(
  const network_address& address)
{
#ifdef _WIN32
  auto s = WSASocket(address.family(), SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED);
  if (INVALID_SOCKET == s) {
    const auto error = WSAGetLastError();
    throw std::system_error(error, std::system_category(), "create socket");
  }
  // Connect 
  if (SOCKET_ERROR == ::connect(s, address, address.size())) {
    // As we are in non-blocking mode we'll always have the error 
    // WSAEWOULDBLOCK whichis actually not one 
    auto error = WSAGetLastError();
    if (WSAEWOULDBLOCK != error) {
      closesocket(s);
      throw std::system_error(error, std::system_category(), "connect");
    }
  }
#else
  auto s = socket(address.family(), SOCK_STREAM, 0);
  if(0 > s) {
    auto error = errno;
    throw std::system_error(error, std::generic_category(), "create socket");
  }
  if (0 > ::connect(s, address, address.size())) {
    auto error = errno;
    ::close(s);
    throw std::system_error(error, std::generic_category(), "connect");
  }
  //make_socket_non_blocking()
  auto flags = fcntl(s, F_GETFL, 0);
  if (flags == -1) {
    ::close(s);
    throw std::runtime_error("fcntl");
  }

  flags |= O_NONBLOCK;
  auto res = fcntl(s, F_SETFL, flags);
  if (res == -1) {
    ::close(s);
    throw std::runtime_error("fcntl");
  }

  //set_timeouts()
  int optval = 1;
  socklen_t optlen = sizeof(optval);
  if (setsockopt(s, SOL_SOCKET, SO_KEEPALIVE, &optval, optlen) < 0) {
    ::close(s);
    throw std::runtime_error("set_timeouts()");
  }

#endif
  return _pool.associate(s, address);
}

void vlib::network::tcp_client::close(connection& c)
{
  _pool.remove(c.socket());
}
