#include <charconv>
#include <sstream>
#include "vlib_stream_with_transfer_encoding.h"

vlib::http::input_stream_with_transfer_encoding::input_stream_with_transfer_encoding(core:: any_ptr<input_stream> target_stream)
  : _eof(false), _target_stream(std::move(target_stream))
{
}

vlib::core::task<size_t> vlib::http::input_stream_with_transfer_encoding::read(void* buffer, size_t size, core::cancellation_token token)
{
  if (_eof) {
    co_return 0;
  }
  auto target_stream = _target_stream.lock();
  for (;;) {
    if (!_current_block) {
      const auto line = co_await target_stream->read_line(4096, token);
      long size;
      if (std::errc::invalid_argument == std::from_chars(line.data(), line.data() + line.size(), size).ec) {
        std::stringstream error;
        error << "Invalid block size " << line << " in the transfer encoding";
        throw std::runtime_error(error.str());
      }
      if (0 == size) {
        _eof = true;
        co_return 0;
      }
      _current_block = std::make_unique<core::input_stream_with_limit>(core
        ::any_ptr<input_stream>(_target_stream, false),
        size);
    }
    const auto readed = co_await _current_block->read(buffer, size, token);
    if (0 != readed) {
      co_return readed;
    }
    
    _current_block.reset();
  }
}
