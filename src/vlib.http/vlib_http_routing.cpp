#include "vlib_http_routing.h"

vlib::http::route_handler::~route_handler()
{
}

vlib::http::simple_route::simple_route(
  std::string method,
  std::string pattern,
  vlib::core::moveonly_function<vlib::core::task<void>(http_request& request, http_response& response, http_context& context)> callback)
  : _method(std::move(method)), _pattern(std::move(pattern)), _callback(std::move(callback)) {
}

bool vlib::http::simple_route::filter(
  const http_request& request,
  const http_context& context) const {
  return _method == request.method()
    && _pattern == request.url();
}

vlib::core::task<void> vlib::http::simple_route::execute(
  http_request& request,
  http_response& response,
  http_context& context) {
  return _callback(request, response, context);
}
