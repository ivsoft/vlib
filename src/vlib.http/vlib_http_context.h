/*
Copyright (c) 2022, Vadim Malyshev, lboss75@gmail.com
All rights reserved
*/

#pragma once

#include "vlib_cancellation_token.h"
#include "vlib_service_provider.h"

namespace vlib {
  namespace http {
    class http_context {
    public:
      http_context(
        std::shared_ptr<core::service_provider> sp,
        core::cancellation_token cancellation_token);

      const std::shared_ptr<core::service_provider>& service_provider() const {
        return _sp;
      }

      const core::cancellation_token& cancellation_token() const {
        return _cancellation_token;
      }

    private:
      std::shared_ptr<core::service_provider> _sp;
      core::cancellation_token _cancellation_token;
    };
  }
}