/*
Copyright (c) 2022, Vadim Malyshev, lboss75@gmail.com
All rights reserved
*/

#pragma once

#include "vlib_service_provider.h"
#include "vlib_task.h"
#include "vlib_http_context.h"
#include "vlib_http_request.h"
#include "vlib_http_response.h"


namespace vlib {
  namespace http {
    class route_handler {
    public:
      virtual ~route_handler();
      
      virtual bool filter(
        const http_request & request,
        const http_context & context) const = 0;

      virtual core::task<void> execute(
        http_request& request,
        http_response& response,
        http_context& context) = 0;
    };

    class simple_route : public route_handler {
    public:
      simple_route(
        std::string method,
        std::string pattern,
        vlib::core::moveonly_function<
        vlib::core::task<void>(
          http_request & request,
          http_response& response,
          http_context & context
          )> callback);

      bool filter(
        const http_request& request,
        const http_context& context) const override;

      core::task<void> execute(
        http_request& request,
        http_response& response,
        http_context& context) override;

    private:
      std::string _method;
      std::string _pattern;
      vlib::core::moveonly_function<
        vlib::core::task<void>(
          http_request& request,
          http_response& response,
          http_context& context
          )> _callback;
    };
  }
}