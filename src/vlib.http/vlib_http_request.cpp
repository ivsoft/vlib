#include "vlib_http_request.h"

vlib::core::task<void> vlib::http::http_request::parse_headers(core::input_stream& stream, core::cancellation_token token)
{
  return _headers.read(stream, token);
}