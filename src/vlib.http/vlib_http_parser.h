/*
Copyright (c) 2022, Vadim Malyshev, lboss75@gmail.com
All rights reserved
*/

#pragma once

#include <string>
#include <string_view>
#include <list>
#include "vlib_task.h"
#include "vlib_stream.h"

namespace vlib {
  namespace http {
    class http_parser {
    public:
      core::task<void> read(
        core::input_stream & stream,
        core::cancellation_token cancellation_token);

      std::list<std::string_view> get_header(const std::string_view& name);

      std::unique_ptr<core::input_stream> get_stream(core::input_stream_with_buffer& stream);
    private:
      std::list<std::string> _headers;
    };
  }
}