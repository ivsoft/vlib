#include "vlib_http_context.h"

vlib::http::http_context::http_context(std::shared_ptr<core::service_provider> sp, core::cancellation_token cancellation_token)
  : _sp(std::move(sp)), _cancellation_token(std::move(cancellation_token)) {
}