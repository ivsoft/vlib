#include "vlib_http_uri.h"

bool vlib::http::uri::try_parse(
  std::string address) noexcept
{
  url = std::move(address);

  auto p = url.find(':');
  if (std::string::npos == p
    || p + 2 > url.length()
    || '/' != url[p + 1]
    || '/' != url[p + 2]) {
    return false;
  }

  schema = std::string_view(url.data(), p);
  auto start = p + 3;

  p = url.find('/', start);
  if (std::string::npos == p) {
    auto p1 = url.find_last_of(':');
    if (std::string::npos == p1 || p1 < start) {
      server = std::string_view(url.data() + start, url.length() - start);
    }
    else {
      server = std::string_view(url.data() + start, p1 - start);
      port = std::string_view(url.data() + p1 + 1, url.length() - p1 - 1);
    }
  }
  else {
    auto p1 = url.find_last_of(':');
    if (std::string::npos == p1 || p1 < start) {
      server = std::string_view(url.data() + start, p - start);
    }
    else if (p < p1) {
      server = std::string_view(url.data() + start, p - start);
    }
    else {
      server = std::string_view(url.data() + start, p1 - start);
      port = std::string_view(url.data() + p1 + 1, p - p1 - 1);
    }

    path = std::string_view(url.data() + p, url.length() - p);
  }
  return true;
}
