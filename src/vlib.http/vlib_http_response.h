#pragma once

/*
Copyright (c) 2022, Vadim Malyshev, lboss75@gmail.com
All rights reserved
*/

#include <string_view>
#include "vlib_task.h"
#include "vlib_cancellation_token.h"
#include <vlib_stream.h>

namespace vlib {
  namespace http {
    class http_response {
    public:
      http_response(core::output_stream & stream);

      core::task<void> send(const std::string_view& body, const core::cancellation_token& cancellation_token);

      core::task<void> send_status(int status, const std::string_view& comment, const std::string_view& httpVersion, const core::cancellation_token& cancellation_token);

      core::task<void> send_headers(const std::list<std::string> & headers, const core::cancellation_token& cancellation_token);
      core::task<void> send_headers(const core::cancellation_token& cancellation_token);
      void add_header(const std::string_view& header);

    private:
      core::output_stream& _stream;
      std::list<std::string> _headers;
    };
  }
}