#include <sstream>
#include "vlib_http_response.h"

vlib::http::http_response::http_response(core::output_stream& stream)
  : _stream(stream) {
}

vlib::core::task<void> vlib::http::http_response::send(const std::string_view& body, const core::cancellation_token& cancellation_token)
{
  co_await send_status(200, "OK", "HTTP/1.1", cancellation_token);
  add_header("Content-Lenght:" + std::to_string(body.length()));

  co_await send_headers(cancellation_token);

  co_await _stream.write_all(body.data(), body.size(), cancellation_token);
}

vlib::core::task<void> vlib::http::http_response::send_status(int status, const std::string_view& comment, const std::string_view& httpVersion, const core::cancellation_token& cancellation_token)
{
  std::stringstream buffer;
  buffer << httpVersion << " " << status << " " << comment << "\r\n";
  const auto result = buffer.str();
  return _stream.write_all(result.data(), result.size(), cancellation_token);
}

void vlib::http::http_response::add_header(const std::string_view& header) {
  _headers.emplace_back(header);
}

vlib::core::task<void> vlib::http::http_response::send_headers(const core::cancellation_token& cancellation_token)
{
  return send_headers(_headers, cancellation_token);
}

vlib::core::task<void> vlib::http::http_response::send_headers(const std::list<std::string>& headers, const core::cancellation_token& cancellation_token)
{
  std::stringstream buffer;
  for (const auto& header : headers) {
    buffer << header << "\r\n";
  }
  buffer << "\r\n";

  const auto result = buffer.str();
  return _stream.write_all(result.data(), result.size(), cancellation_token);
}