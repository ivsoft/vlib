/*
Copyright (c) 2022, Vadim Malyshev, lboss75@gmail.com
All rights reserved
*/

#pragma once

#include <string_view>
#include "vlib_task.h"
#include "vlib_stream.h"
#include "vlib_http_parser.h"

namespace vlib {
  namespace http {
    class http_request {
    public:
      const std::string& method() const { return _method; }
      const std::string& url() const { return _url; }
      const std::string& httpVersion() const { return _httpVersion; }

      void method(std::string value) { _method = std::move(value); }
      void url(std::string value) { _url = std::move(value); }
      void httpVersion(std::string value) { _httpVersion = std::move(value); }

      core::task<void> parse_headers(core::input_stream& stream, core::cancellation_token token);

    private:
      std::string _method;
      std::string _url;
      std::string _httpVersion;
      http_parser _headers;
    };
  }
}