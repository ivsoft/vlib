#include <charconv>
#include <unordered_set>
#include "vlib_http_parser.h"
#include "vlib_string_utils.h"
#include "vlib_stream_with_transfer_encoding.h"

vlib::core::task<void> vlib::http::http_parser::read(
  core::input_stream& stream,
  core::cancellation_token cancellation_token) {
  for (;;) {
    const auto line = co_await stream.read_line(4096, cancellation_token);
    const auto line_view = core::trim(line);
    if (line_view.empty()) {
      co_return;
    }
    _headers.emplace_back(line_view);
  }
}

std::list<std::string_view> vlib::http::http_parser::get_header(const std::string_view& name) {
  std::list<std::string_view> result;
  for (auto& p : _headers) {
    if (
      p.size() > name.size()
      && p[name.size()] == ':'
      && !p.compare(0, name.size(), name)) {
      result.emplace_back(
        core::trim(
          std::string_view(
            p.data() + name.size() + 1,
            p.size() - name.size() - 1)));
    }
  }
  return result;
}

std::unique_ptr<vlib::core::input_stream> vlib::http::http_parser::get_stream(core::input_stream_with_buffer& stream) {
  std::unique_ptr<core::input_stream> result;

  const auto transfer_encoding = get_header("Transfer-Encoding");
  if (!transfer_encoding.empty()) {
    std::unordered_set<std::string_view> encodings;
    for (const auto& header : transfer_encoding) {
      for (const auto& encoding : core::split_string(header, ",")) {
        const auto enc = core::trim(encoding);
        if (encodings.end() == encodings.find(enc)) {
          encodings.emplace(enc);
        }
      }
    }
    if (encodings.end() == encodings.find("chunked")) {
      result = std::make_unique<input_stream_with_transfer_encoding>(&stream);
    }
  }

  if (!result) {
    const auto content_length = get_header("Content-Length");
    if (!content_length.empty()) {
      const auto& header = content_length.front();
      long size;
      if (std::errc::invalid_argument == std::from_chars(header.data(), header.data() + header.size(), size).ec) {
        std::stringstream error;
        error << "Invalid Content-Length header " << header;
        throw std::runtime_error(error.str());
      }

      result = std::make_unique<core::input_stream_with_limit>(&stream, size);
    }
  }

  if (!result) {
    throw std::runtime_error("Invalid headers at http message");
  }

  return result;
}
