/*
Copyright (c) 2022, Vadim Malyshev, lboss75@gmail.com
All rights reserved
*/

#pragma once

#include <string_view>
#include "vlib_task.h"
#include "vlib_cancellation_token.h"
#include "vlib_connection_pool.h"
#include "vlib_tcp_client.h"
#include "vlib_stream.h"
#include "vlib_http_parser.h"

namespace vlib {
  namespace http {
    class http_client_request {
    public:
      http_client_request(
        std::string_view url,
        network::tcp_client client,
        network::connection& connection);

      core::task<std::string> responseProtocol(core::cancellation_token cancellation_token);
      core::task<int> responseStatusCode(core::cancellation_token cancellation_token);
      core::task<std::string> responseComment(core::cancellation_token cancellation_token);

      core::task<void> ensureStatus(core::cancellation_token cancellation_token);

      core::task<std::string> response_as_string(core::cancellation_token cancellation_token);
      core::task<std::unique_ptr<core::input_stream>> response_stream(core::cancellation_token cancellation_token);

      core::task<void> close(core::cancellation_token cancellation_token);

    private:
      std::string _url;

      network::tcp_client _client;
      network::connection& _connection;
      core::input_stream_with_buffer _input_stream;

      bool _headersSent;
      std::string _responseProtocol;
      int _responseStatusCode;
      std::string _responseComment;

      http_parser _parser;

      core::task<void> ensureHeadersSent(core::cancellation_token cancellation_token);
    };

    class http_client {
    public:
      http_client(network::connection_pool& pool);

      core::task<http_client_request> send(
        std::string_view method,
        std::string url,
        core::cancellation_token cancellation_token);

    private:
      network::connection_pool& _pool;
      std::string _protocolVersion;
    };
  }
}