#include "vlib_http_server_host.h"
#include "vlib_tcp_server_host.h"
#include "vlib_http_parser.h"
#include "vlib_string_utils.h"
#include "vlib_logger.h"

void vlib::http::http_server_host::addRouting(vlib::core::service_collection& services) {
  services.add_singleton<http_server_host>([](vlib::core::service_provider & sp) {
    return std::make_shared<http_server_host>();
  });
}

void vlib::http::http_server_host::addHandler(std::unique_ptr<route_handler> handler)
{
  _handlers.emplace_back(std::move(handler));
}

void vlib::http::http_server_host::useRouting(const std::shared_ptr<vlib::core::service_provider>& sp) {
  sp->get_required<network::tcp_server_host_manager>()->add_handler([sp](network::connection& connection, core::cancellation_token token) -> core::task<bool> {
    co_await sp->get_required<http_server_host>()->new_connection(sp, connection, std::move(token));
    co_return true;
  });
}

void vlib::http::http_server_host::mapUrl(
  const std::shared_ptr<vlib::core::service_provider>& sp,
  std::string method,
  std::string pattern,
  vlib::core::moveonly_function<vlib::core::task<void>(http_request& request, http_response& response, http_context& context)> callback){
  sp->get_required<http_server_host>()->addHandler(
    std::make_unique<simple_route>(
      std::move(method),
      std::move(pattern),
      std::move(callback)));
}

vlib::core::task<void> vlib::http::http_server_host::new_connection(
  std::shared_ptr<vlib::core::service_provider> sp,
  network::connection& connection,
  core::cancellation_token cancellation_token)
{
  core::logger logger(sp, "http_server");

  try {
    co_await logger.log(core::log_level::debug, "New connection");

    core::input_stream_with_buffer input_stream(&connection.input_stream());

    std::string requestStr = co_await input_stream.read_line(4096, cancellation_token);

    co_await logger.log(core::log_level::debug, "Request '" + requestStr + "'");

    const auto request_parts = core::split_string(requestStr, 3);
    auto p = request_parts.begin();
    if (request_parts.end() == p) {
      throw std::runtime_error("Invalid request '" + requestStr + "'");
    }
    http_request request;
    request.method(std::string{ *p });
    ++p;
    if (request_parts.end() == p) {
      throw std::runtime_error("Invalid request '" + requestStr + "'");
    }
    request.url(std::string{ *p });
    ++p;
    if (request_parts.end() != p) {
      request.httpVersion(std::string{ core::trim_right(*p, "\r") });
    }

    co_await request.parse_headers(input_stream, cancellation_token);

    http_response response(connection.output_stream());
    http_context context(sp, cancellation_token);
    bool handled = false;
    for (auto& route : _handlers) {
      if (route->filter(request, context)) {
        co_await route->execute(request, response, context);
        handled = true;
        break;
      }
    }
    if (!handled) {
      co_await response.send_status(404, "Not found", "HTTP/1.1", cancellation_token);
    }
  }
  catch (const std::exception& ex) {
    std::stringstream error;
    error << "HTTP/1.1 503 " << ex.what() << "\r\n";
    const auto result = error.str();
    connection.output_stream().write_all(result.data(), result.size(), cancellation_token).get();
  }
  co_return;
}

