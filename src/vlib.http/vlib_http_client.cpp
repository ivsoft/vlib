#include <charconv>
#include <sstream>
#include "vlib_http_client.h"
#include "vlib_tcp_client.h"
#include "vlib_http_uri.h"
#include "vlib_string_utils.h"
#include "vlib_http_parser.h"

vlib::http::http_client::http_client(
  network::connection_pool& pool)
  : _pool(pool), _protocolVersion("HTTP/1.1") {
}

vlib::core::task<vlib::http::http_client_request> vlib::http::http_client::send(
  std::string_view method,
  std::string url,
  core::cancellation_token cancellation_token){

  uri address;
  if (!address.try_parse(url) || "http" != address.schema) {
    throw std::runtime_error("Invalid URL " + url);
  }

  vlib::network::tcp_client client(_pool);

  int port;
  if(std::errc::invalid_argument == std::from_chars(address.port.data(), address.port.data() + address.port.size(), port).ec) {
    throw std::runtime_error("Invalid port in the URL " + url);
  }
  auto tcp_address = vlib::network::network_address::tcp_ip4(address.server, port);
  auto& s = client.connect(tcp_address);

  co_await s.output_stream().write_all(method.data(), method.size(), cancellation_token);
  co_await s.output_stream().write_all(" ", 1, cancellation_token);
  co_await s.output_stream().write_all(address.path.data(), address.path.size(), cancellation_token);
  co_await s.output_stream().write_all(" ", 1, cancellation_token);
  co_await s.output_stream().write_all(_protocolVersion.data(), _protocolVersion.size(), cancellation_token);
  co_await s.output_stream().write_all("\n", 1, cancellation_token);

  co_return http_client_request(url, std::move(client), s);
}

vlib::http::http_client_request::http_client_request(
  std::string_view url,
  network::tcp_client client,
  network::connection& connection)
  : _url(url), _client(std::move(client)), _connection(connection), _headersSent(false),
  _input_stream(&connection.input_stream()) {
}

vlib::core::task<void> vlib::http::http_client_request::ensureHeadersSent(vlib::core::cancellation_token cancellation_token) {
  if (!_headersSent) {
    co_await _connection.output_stream().write_all("\n", 1, cancellation_token);

    std::string response;
    try {
      response = co_await _input_stream.read_line(4096, cancellation_token);
    }
    catch (const std::exception& ex) {
      std::stringstream error;
      error << ex.what() << " at read server " << _url << " answer";
      throw std::runtime_error(error.str());
    }

    const auto response_parts = core::split_string(response, 3);
    auto p = response_parts.begin();
    if (response_parts.end() == p) {
      std::stringstream error;
      error << "Invalid server '" << _url << "' response '" << response << "'";
      throw std::runtime_error(error.str());
    }
    _responseProtocol = *p;
    ++p;
    if (response_parts.end() == p) {
      throw std::runtime_error("Invalid server '" + _url + "' response " + response);
    }
    if (std::errc::invalid_argument == std::from_chars(p->data(), p->data() + p->size(), _responseStatusCode).ec) {
      std::stringstream error;
      error << "Invalid server '" << _url << "' status code in the response '" << response << "'";
      throw std::runtime_error(error.str());
    }
    ++p;
    if (response_parts.end() != p) {
      _responseComment = *p;
    }

    co_await _parser.read(_input_stream, cancellation_token);
    const auto expect = _parser.get_header("Expect");
    if (!expect.empty() && "100-continue" == expect.front()) {
      co_await _connection.output_stream().write_all("100 Continue\n", 1, cancellation_token);
    }

    _headersSent = true;
  }
}

vlib::core::task<std::string> vlib::http::http_client_request::responseProtocol(core::cancellation_token cancellation_token) {
  co_await ensureHeadersSent(cancellation_token);
  co_return _responseProtocol;
}


vlib::core::task<int> vlib::http::http_client_request::responseStatusCode(core::cancellation_token cancellation_token) {
  co_await ensureHeadersSent(cancellation_token);
  co_return _responseStatusCode;
}

vlib::core::task<std::string> vlib::http::http_client_request::responseComment(core::cancellation_token cancellation_token) {
  co_await ensureHeadersSent(cancellation_token);
  co_return _responseComment;
}

vlib::core::task<void> vlib::http::http_client_request::ensureStatus(core::cancellation_token cancellation_token) {
  co_await ensureHeadersSent(cancellation_token);
  if (200 < _responseStatusCode && _responseStatusCode < 400){
    co_return;
  }
  std::stringstream error;
  error << "Invalid server '" << _url << "' status code " << _responseStatusCode;
  throw std::runtime_error(error.str());
}

vlib::core::task<std::unique_ptr<vlib::core::input_stream>> vlib::http::http_client_request::response_stream(
  core::cancellation_token cancellation_token) {
  co_await ensureStatus(cancellation_token);


  co_return _parser.get_stream(_input_stream);
}