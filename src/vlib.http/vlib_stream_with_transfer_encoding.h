/*
Copyright (c) 2022, Vadim Malyshev, lboss75@gmail.com
All rights reserved
*/
#pragma once

#include "vlib_stream.h"
#include "vlib_any_ptr.h"

namespace vlib {
  namespace http {
    class input_stream_with_transfer_encoding : public core::input_stream {
    public:
      input_stream_with_transfer_encoding(core::any_ptr<input_stream> target_stream);

      core::task<size_t> read(void* buffer, size_t size, core::cancellation_token token) override;

    private:
      bool _eof;
      std::unique_ptr<input_stream> _current_block;
      core::any_ptr<input_stream> _target_stream;
    };
  }
}