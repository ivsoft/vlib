/*
Copyright (c) 2022, Vadim Malyshev, lboss75@gmail.com
All rights reserved
*/
#pragma once

#include <string>
#include <string_view>

namespace vlib {
  namespace http {
    class uri {
    public:
      std::string url;

      std::string_view schema;
      std::string_view server;
      std::string_view port;
      std::string_view path;

      bool try_parse(std::string address) noexcept;
    };
  }
}