/*
Copyright (c) 2022, Vadim Malyshev, lboss75@gmail.com
All rights reserved
*/

#pragma once

#include "vlib_service_provider.h"
#include "vlib_task.h"
#include "vlib_http_context.h"
#include "vlib_http_request.h"
#include "vlib_http_response.h"
#include "vlib_connection.h"
#include "vlib_http_routing.h"

namespace vlib {
  namespace http {
    class http_server_host {
    public:
      static void addRouting(vlib::core::service_collection& services);

      static void useRouting(
        const std::shared_ptr<vlib::core::service_provider>& service_provider);

      static void mapUrl(
        const std::shared_ptr<vlib::core::service_provider>& sp,
        std::string method,
        std::string pattern,
        vlib::core::moveonly_function<
          vlib::core::task<void>(
            http_request& request,
            http_response& response,
            http_context& context
            )> callback);

    private:
      std::list<std::unique_ptr<route_handler>> _handlers;

      core::task<void> new_connection(std::shared_ptr<vlib::core::service_provider> sp, network::connection& connection, core::cancellation_token token);
      void addHandler(std::unique_ptr<route_handler> handler);

      
    };
  }
}