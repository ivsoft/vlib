#pragma once

#include <future>
#include <optional>
#include "vlib_moveonly_function.h"
#include "vlib_thread_pool.h"
#include <assert.h>
#include <coroutine>
#include <concepts>

namespace vlib {
  namespace core {

    class task_pool {
    public:
      task_pool() noexcept;

      static task_pool* shared_task_pool;

      void add(moveonly_function_noexcept<void(void)> callback) noexcept;
      void wait_all() noexcept { _thread_pool.wait_all(); }

    private:
      thread_pool_noexcept _thread_pool;
    };

    template<typename result_type>
    class task;

    template<>
    class task<void> {
      private:
        struct future_result
        {
          std::future<void> _future;
          moveonly_function<void(void)> _next;
          std::mutex _next_mutex;

          future_result(std::future<void> future) noexcept
            : _future(std::move(future)) {
          }
          ~future_result() {
            _next_mutex.lock();//wait complete async opertaions
            _next_mutex.unlock();
          }

          bool ready() const noexcept {
            return (_future.wait_for(std::chrono::seconds(0)) == std::future_status::ready);
          }

          void get() {
            _future.get();
          }

          bool then(moveonly_function<void(void)> next)  noexcept {
            std::lock_guard<std::mutex> lock(_next_mutex);

            if (ready()) {
              return false;
            }

            _next = std::move(next);
            return true;
          }

          void then_always(moveonly_function<void(void)> next)  noexcept {
            std::unique_lock<std::mutex> lock(_next_mutex);

            if (ready()) {
              lock.unlock();

              next();
              return;
            }

            _next = std::move(next);
            return;
          }

        };

        class future {
        public:
          future(std::shared_ptr<future_result> result) noexcept
            : _result(std::move(result)) {
          }

          future(future&&)  noexcept = default;
          future& operator =(future&&)  noexcept = default;

          bool ready() const  noexcept {
            //std::lock_guard<std::mutex> lock(_result->_next_mutex);
            return _result->ready();
          }

          void get() {
            _result->get();
          }

          bool then(moveonly_function<void(void)> next)  noexcept {
            return _result->then(std::move(next));
          }
          void then_always(moveonly_function<void(void)> next)  noexcept {
            _result->then_always(std::move(next));
          }

        private:
          std::shared_ptr<future_result> _result;
        };

        class promise {
        public:
          promise()
            : _promise_raised(false)
          {
            _result = std::make_shared<future_result>(_promise.get_future());
          }

          future get_future()  noexcept {
            return future(_result);
          }

          bool return_void()  noexcept {
            std::unique_lock<std::mutex> lock(_result->_next_mutex);
            if (_promise_raised) {
              return false;
            }
            _promise_raised = true;
            _promise.set_value();

            if (_result->_next) {
              moveonly_function<void(void)> callback = std::move(_result->_next);
              lock.unlock();

              callback();
            }

            return true;
          }
          bool set_exception(std::exception_ptr ex) noexcept {
            std::unique_lock<std::mutex> lock(_result->_next_mutex);
            if (_promise_raised) {
              return false;
            }
            _promise_raised = true;
            _promise.set_exception(std::move(ex));

            if (_result->_next) {
              moveonly_function<void(void)> callback = std::move(_result->_next);
              lock.unlock();

              callback();
            }
            return true;
          }

          void reset() noexcept {
            _promise = std::promise<void>();
            _result = std::make_shared<future_result>(_promise.get_future());
            _promise_raised = false;
          }

        private:
          bool _promise_raised;
          std::promise<void> _promise;
          std::shared_ptr<future_result> _result;
        };

        future _f;
      public:
        class promise_type {
        private:
          promise _p;

        public:
          promise_type() noexcept {}
          promise_type(const promise_type&) = delete;
          promise_type(promise_type&&) = delete;
          promise_type& operator =(const promise_type&) = delete;
          promise_type& operator =(promise_type&&) = delete;

          ~promise_type() noexcept {}

          task get_return_object() noexcept { return task(_p.get_future()); }

          std::suspend_never initial_suspend() noexcept { return {}; }
          std::suspend_never final_suspend() noexcept { return {}; }

          void return_void() noexcept {
            _p.return_void();
          }
          void unhandled_exception() noexcept {
            try_set_exception(std::current_exception());
          }
          void set_exception(std::exception_ptr ex) noexcept {
            try_set_exception(std::move(ex));
          }


          bool try_return_void() noexcept {
            return _p.return_void();
          }
          bool try_set_exception(std::exception_ptr ex) noexcept {
            return _p.set_exception(std::move(ex));
          }

          void reset() noexcept { _p.reset(); }
        };


        task() = delete;
        task(const task&) = delete;
        task& operator = (const task&) = delete;

        task(task&& original) = default;
        task& operator = (task&&) = default;

        task(future f) noexcept
          : _f(std::move(f)) {}

        bool await_ready() noexcept {
          return _f.ready();
        }

        auto await_resume() {
          return _f.get();
        }
        template<typename handle_type>
        bool await_suspend(handle_type next) noexcept {
          return _f.then([next]() mutable noexcept {
#ifdef TASK_USE_TASK_POOL
            task_pool::shared_task_pool->add([next]() mutable noexcept {
              next();
              });
#else
            next();
#endif
            });
        }

        void then(moveonly_function_noexcept<void(void)> callback) noexcept {
          _f.then_always(std::move(callback));
        }

        void then(moveonly_function_noexcept<void(task t)> callback) noexcept {
          auto tmp = std::make_shared<task>(std::move(*this));
          tmp->then([tmp, c = std::move(callback)]() noexcept {
            c(std::move(*tmp));
          });
        }

        void get() {
          _f.get();
        }

        static task completed() noexcept {
          promise_type p{};
          p.return_void();
          return p.get_return_object();
        }
      };

    template<typename result_type>
    class task {
    private:
      struct future_result
      {
        std::future<result_type> _future;
        moveonly_function<void(void)> _next;
        std::mutex _next_mutex;

        future_result(std::future<result_type> future) noexcept
          : _future(std::move(future)) {
        }

        ~future_result() {
          _next_mutex.lock();//wait complete async opertaions
          _next_mutex.unlock();
        }

        bool ready() const noexcept {
          return (_future.wait_for(std::chrono::seconds(0)) == std::future_status::ready);
        }

        result_type get() {
          return _future.get();
        }

        bool then(moveonly_function<void(void)> next)  noexcept {
          std::lock_guard<std::mutex> lock(_next_mutex);

          if (ready()) {
            return false;
          }

          _next = std::move(next);
          return true;
        }

        void then_always(moveonly_function<void(void)> next)  noexcept {
          std::unique_lock<std::mutex> lock(_next_mutex);

          if (ready()) {
            lock.unlock();

            next();
            return;
          }

          _next = std::move(next);
          return;
        }

      };

      class future {
      public:
        future(std::shared_ptr<future_result> result) noexcept
          : _result(std::move(result)) {
        }

        future(future&&)  noexcept = default;
        future& operator =(future&&)  noexcept = default;

        bool ready() const noexcept {
          //std::lock_guard<std::mutex> lock(_result->_next_mutex);
          return _result->ready();
        }

        result_type get() {
          return _result->get();
        }

        bool then(moveonly_function<void(void)> next)  noexcept {
          return _result->then(std::move(next));
        }

        void then_always(moveonly_function<void(void)> next)  noexcept {
          _result->then_always(std::move(next));
        }

      private:
        std::shared_ptr<future_result> _result;
      };

      class promise {
      public:
        promise()
          : _promise_raised(false)
        {
          _result = std::make_shared<future_result>(_promise.get_future());
        }

        future get_future()  noexcept {
          return future(_result);
        }

        template<typename U>
        bool set_value(U&& u)  noexcept {
          std::unique_lock<std::mutex> lock(_result->_next_mutex);
          if (_promise_raised) {
            return false;
          }
          _promise_raised = true;
          _promise.set_value(std::forward<U>(u));

          if (_result->_next) {
            auto callback = std::move(_result->_next);
            lock.unlock();

            callback();
          }
          return true;
        }
        bool set_exception(std::exception_ptr ex) noexcept {
          std::unique_lock<std::mutex> lock(_result->_next_mutex);
          if (_promise_raised) {
            return false;
          }
          _promise_raised = true;
          _promise.set_exception(std::move(ex));

          if (_result->_next) {
            auto callback = std::move(_result->_next);
            lock.unlock();

            callback();
          }
          return true;
        }

        void reset() noexcept {
          _promise = std::promise<result_type>();
          _result = std::make_shared<future_result>(_promise.get_future());
          _promise_raised = false;
        }

      private:
        bool _promise_raised;
        std::promise<result_type> _promise;
        std::shared_ptr<future_result> _result;
      };

      future _f;
    public:
      class promise_type {
      private:
        promise _p;

      public:
        promise_type() noexcept {}
        promise_type(const promise_type&) = delete;
        promise_type(promise_type&&) = delete;
        promise_type& operator =(const promise_type&) = delete;
        promise_type& operator =(promise_type&&) = delete;

        ~promise_type() noexcept {}

        task get_return_object() noexcept { return task(_p.get_future()); }

        std::suspend_never initial_suspend() noexcept { return {}; }
        std::suspend_never final_suspend() noexcept { return {}; }

        template<std::convertible_to<result_type> U> // C++20 concept
        void return_value(U&& u) noexcept {
          try_return_value(std::forward<U>(u));
        }
        void unhandled_exception() noexcept {
          try_set_exception(std::current_exception());
        }
        void set_exception(std::exception_ptr ex) noexcept {
          try_set_exception(std::move(ex));
        }

        template<std::convertible_to<result_type> U> // C++20 concept
        bool try_return_value(U&& u) noexcept {
          return _p.set_value(std::forward<U>(u));
        }
        bool try_set_exception(std::exception_ptr ex) noexcept {
          return _p.set_exception(ex);
        }
        void reset() noexcept {
          _p.reset();
        }
      };


      task() = delete;
      task(const task &) = delete;
      task& operator = (const task&) = delete;

      task(task&& original) = default;
      task& operator = (task&&) = default;

      task(future f) noexcept
        : _f(std::move(f)) {}

      bool await_ready() noexcept {
        return _f.ready();
      }

      auto await_resume() {
        return _f.get();
      }
      template<typename handle_type>
      bool await_suspend(handle_type next) noexcept {
        return _f.then([next]() mutable noexcept {
#ifdef TASK_USE_TASK_POOL
          task_pool::shared_task_pool->add([next]() mutable noexcept {
            next.resume();
            });
#else
          next.resume();
#endif
          });
      }

      void then(moveonly_function_noexcept<void(void)> callback) noexcept {
        _f.then_always(std::move(callback));
      }

      void then(moveonly_function_noexcept<void(task t)> callback) noexcept {
        auto tmp = std::make_shared<task>(std::move(*this));
        tmp->then([tmp, c = std::move(callback)]() noexcept {
          c(std::move(*tmp));
        });
      }

      result_type get() {
        return _f.get();
      }

      template<std::convertible_to<result_type> U> // C++20 concept
      static task completed(U && u) noexcept {
        promise_type p {};
        p.return_value(std::forward<U>(u));
        return p.get_return_object();
      }
    };
  }
}