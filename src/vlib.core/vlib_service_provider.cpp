#include "vlib_service_provider.h"

vlib::core::service_description::~service_description() noexcept
{
}

vlib::core::service_provider::service_provider(service_description::service_map && services)
  : _services(std::move(services)) {
}
vlib::core::service_provider::service_provider(std::shared_ptr<service_provider> parent, service_description::service_map && services)
  : _parent(std::move(parent)), _services(std::move(services)) {
}

std::shared_ptr<vlib::core::service_provider> vlib::core::service_collection::build() {
  return std::make_shared<service_provider>(std::move(_services));
}

std::shared_ptr<vlib::core::service_provider> vlib::core::service_provider::create_scope() {
  service_description::service_map scope_services;
  for (const auto & service : _services) {
    for (const auto& factory : service.second) {
      factory->get_scope_service(scope_services);
    }
  }

  return std::make_shared<service_provider>(shared_from_this(), std::move(scope_services));
}
