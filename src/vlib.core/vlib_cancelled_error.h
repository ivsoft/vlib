#pragma once

#include <exception>
#include <string>

namespace vlib {
  namespace core {
    class cancelled_error : public std::runtime_error {
    public:
      cancelled_error() : std::runtime_error("cancelled") {}
      cancelled_error(const std::string & msg) : std::runtime_error(msg) {}
    };
  }
}