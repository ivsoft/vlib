#include "vlib_host.h"
#include "vlib_logger.h"

vlib::core::host vlib::core::host_builder::build()
{
  if (!_services.has<host_lifetime>()) {
    _services.add_singleton<host_lifetime>([](service_provider& sp) { return std::make_shared<host_lifetime>(); });
  }
  return host(_services.build());
}

vlib::core::host::host(std::shared_ptr<service_provider> sp)
  : _sp(std::move(sp)) {
}

vlib::core::task<void> vlib::core::host::start(cancellation_token token)
{
  for (auto& service : _sp->get_services<hosted_service>()) {
    co_await service->start(token);
  }
  co_await logger(_sp, "host").log(core::log_level::information, "host started");
}
vlib::core::task<void> vlib::core::host::stop(cancellation_token token)
{
  co_await logger(_sp, "host").log(core::log_level::information, "host is shouting down...");
  for (auto& service : _sp->get_services<hosted_service>()) {
    co_await service->stop(token);
  }
}

vlib::core::task<void> vlib::core::host::run(cancellation_token token)
{
  co_await start(token);

  auto lifetime = _sp->get<host_lifetime>();
  co_await lifetime->wait();

  co_await stop(token);

  co_return;
}

vlib::core::hosted_service::~hosted_service()
{
}

vlib::core::host_lifetime::~host_lifetime()
{
}

vlib::core::task<void> vlib::core::host_lifetime::wait()
{
  auto result = std::make_shared<vlib::core::task<void>::promise_type>();
  _source.on_cancel([result]() {
    result->return_void();
    return task<void>::completed();
  });

  return result->get_return_object();
}

void vlib::core::host_lifetime::stop()
{
  _source.cancel();
}
