#pragma once

#include <mutex>
#include <unordered_map>

#include "vlib_moveonly_function.h"
#include "vlib_task.h"

namespace vlib {
  namespace core {
    class cancellation_token_source {
    public:
      bool is_cancelled() const noexcept { return _is_cancel; }

      task<void> cancel() noexcept;

      int on_cancel(moveonly_function_noexcept<task<void>(void)> cancel_handler);

      void remove(int token) noexcept;

    private:
      bool _is_cancel;
      std::mutex _mutex;
      struct handler_info {
        moveonly_function_noexcept<task<void>(void)> _target;
        bool _is_raised;
      };
      std::unordered_map<int, handler_info> _handlers;
    };
  }
}