#pragma once

#include <memory>

namespace vlib {
  namespace core {
    template<typename signature>
    class moveonly_function;

    template<typename signature>
    class moveonly_function_noexcept;

    template<typename result_type, typename... arg_types>
    class moveonly_function<result_type(arg_types...)>
    {
    public:
      moveonly_function() noexcept {
      }

      moveonly_function(moveonly_function&& origin) noexcept
        : holder_(std::move(origin.holder_)) {
      }

      template<typename F, class = typename std::enable_if<std::is_invocable_r_v<result_type, F, arg_types...> && !std::is_same<F, moveonly_function>::value>::type>
      moveonly_function(F f) noexcept
        : holder_(new holder<F>(std::move(f))) {
      }

      result_type operator ()(arg_types... args) const {
        return (*this->holder_)(std::forward<arg_types>(args)...);
      }

      bool operator ! () const noexcept {
        return !this->holder_;
      }

      explicit operator bool() const noexcept {
        return this->holder_.get() != nullptr;
      }

      moveonly_function& operator = (moveonly_function&& original) noexcept {
        this->holder_ = std::move(original.holder_);
        return *this;
      }

      void swap(moveonly_function& original) noexcept {
        auto tmp = std::move(original.holder_);
        original.holder_ = std::move(this->holder_);
        this->holder_ = std::move(tmp);
      }

      void reset() noexcept {
        holder_.reset();
      }

    private:
      class holder_base {
      public:
        virtual ~holder_base() noexcept {}
        virtual result_type operator ()(arg_types... args) = 0;
      };

      template<typename F>
      class holder : public holder_base {
      public:
        holder(F f) noexcept
          : f_(std::move(f)) {
        }

        result_type operator ()(arg_types... args) override {
          return this->f_(std::forward<arg_types>(args)...);
        }

      private:
        F f_;
      };

      std::unique_ptr<holder_base> holder_;
    };

    template<typename result_type, typename... arg_types>
    class moveonly_function_noexcept<result_type(arg_types...)>
    {
    public:
      moveonly_function_noexcept() noexcept {
      }

      moveonly_function_noexcept(moveonly_function_noexcept&& origin) noexcept
        : holder_(std::move(origin.holder_)) {
      }

      template<typename F, class = typename std::enable_if<std::is_invocable_r_v<result_type, F, arg_types...> && !std::is_same<F, moveonly_function_noexcept>::value>::type>
      moveonly_function_noexcept(F f) noexcept
        : holder_(new holder<F>(std::move(f))) {
      }

      result_type operator ()(arg_types... args) const noexcept {
        return (*this->holder_)(std::forward<arg_types>(args)...);
      }

      bool operator ! () const noexcept {
        return !this->holder_;
      }

      explicit operator bool() const noexcept {
        return this->holder_.get() != nullptr;
      }

      moveonly_function_noexcept& operator = (moveonly_function_noexcept&& original) noexcept {
        this->holder_ = std::move(original.holder_);
        return *this;
      }

      void swap(moveonly_function_noexcept& original) noexcept {
        auto tmp = std::move(original.holder_);
        original.holder_ = std::move(this->holder_);
        this->holder_ = std::move(tmp);
      }

      void reset() noexcept {
        holder_.reset();
      }

    private:
      class holder_base {
      public:
        virtual ~holder_base() noexcept {}
        virtual result_type operator ()(arg_types... args) noexcept = 0;
      };

      template<typename F>
      class holder : public holder_base {
      public:
        holder(F f) noexcept
          : f_(std::move(f)) {
        }

        result_type operator ()(arg_types... args) noexcept override {
          return this->f_(std::forward<arg_types>(args)...);
        }

      private:
        F f_;
      };

      std::unique_ptr<holder_base> holder_;
    };
  }
}