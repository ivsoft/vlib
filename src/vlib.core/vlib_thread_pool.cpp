#include "vlib_thread_pool.h"

vlib::core::thread_pool::thread_pool(std::size_t capacity) noexcept
  : _task_in_list(0)
{
  _threads.reserve(capacity);
  for (std::size_t i = 0; i < capacity; ++i) {
    _threads.emplace_back(&thread_pool::work_func, this);
  }
}

void vlib::core::thread_pool::add(moveonly_function<void(void)> callback) noexcept
{
  std::lock_guard<std::mutex> lock(_task_list_mutex);
  _task_list.emplace(std::move(callback));
  ++_task_in_list;

  _task_list_cv.notify_one();
}

void vlib::core::thread_pool::stop() noexcept
{
  _quite = true;
  for (auto& thread : _threads) {
    _task_list_cv.notify_all();
    thread.join();
  }

  _threads.clear();
}

void vlib::core::thread_pool::wait_all() noexcept
{
  for (;;) {
    std::unique_lock<std::mutex> lock(_task_list_mutex);
    _task_list_empty_cv.wait(lock, [this]()->bool { return 0 == _task_in_list; });

    if (0 == _task_in_list) {
      break;
    }
  }
}

void vlib::core::thread_pool::work_func() noexcept
{
  while (!_quite) {
    std::unique_lock<std::mutex> lock(_task_list_mutex);
    _task_list_cv.wait(lock, [this]()->bool { return !_task_list.empty() || _quite; });

    if (!_task_list.empty()) {
      auto elem = std::move(_task_list.front());
      _task_list.pop();
      lock.unlock();

      elem();

      lock.lock();
      if (0 == --_task_in_list) {
        _task_list_empty_cv.notify_one();
      }
    }
  }
}
///////////////////////////////////////////////////////////////////
vlib::core::thread_pool_noexcept::thread_pool_noexcept(std::size_t capacity) noexcept
  : _task_in_list(0)
{
  _threads.reserve(capacity);
  for (std::size_t i = 0; i < capacity; ++i) {
    _threads.emplace_back(&thread_pool_noexcept::work_func, this);
  }
}

void vlib::core::thread_pool_noexcept::add(moveonly_function_noexcept<void(void)> callback) noexcept
{
  std::lock_guard<std::mutex> lock(_task_list_mutex);
  _task_list.emplace(std::move(callback));
  ++_task_in_list;

  _task_list_cv.notify_one();
}

void vlib::core::thread_pool_noexcept::stop() noexcept
{
  _quite = true;
  for (auto& thread : _threads) {
    _task_list_cv.notify_all();
    thread.join();
  }

  _threads.clear();
}

void vlib::core::thread_pool_noexcept::wait_all() noexcept
{
  for (;;) {
    std::unique_lock<std::mutex> lock(_task_list_mutex);
    _task_list_empty_cv.wait(lock, [this]()->bool { return 0 == _task_in_list; });

    if (0 == _task_in_list) {
      break;
    }
  }
}

void vlib::core::thread_pool_noexcept::work_func() noexcept
{
  while (!_quite) {
    std::unique_lock<std::mutex> lock(_task_list_mutex);
    _task_list_cv.wait(lock, [this]()->bool { return !_task_list.empty() || _quite; });

    if (!_task_list.empty()) {
      auto elem = std::move(_task_list.front());
      _task_list.pop();
      lock.unlock();

      elem();

      lock.lock();
      if (0 == --_task_in_list) {
        _task_list_empty_cv.notify_one();
      }
    }
  }
}


