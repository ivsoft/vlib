/*
Copyright (c) 2022, Vadim Malyshev, lboss75@gmail.com
All rights reserved
*/

#pragma once

#include <memory>
#include <variant>

namespace vlib {
  namespace core {
    template <typename target_type>
    class locked_any_ptr {
    public:
      locked_any_ptr(target_type* ptr) : _ptr(ptr) {}
      locked_any_ptr(std::shared_ptr<target_type> holder) : _ptr(holder.get()), _holder(std::move(holder)) {}

      target_type* get() const { return _ptr; }
      target_type* operator ->() const { return _ptr; }

    private:
      target_type* _ptr;
      std::shared_ptr<target_type> _holder;
    };

    template <typename target_type>
    class any_ptr {
    public:
      any_ptr(target_type * ptr) : _holder(ptr) {}
      
      any_ptr(any_ptr<target_type>&&) = default;
      any_ptr(const any_ptr<target_type>&) = delete;

      any_ptr(any_ptr<target_type>& other, bool transfer) {
        if (std::holds_alternative<target_type *>(other._holder)) {
          _holder.emplace<target_type*>(std::get<target_type*>(other._holder));
        }
        else if (std::holds_alternative<std::weak_ptr<target_type>>(other._holder)) {
          _holder.emplace<std::weak_ptr<target_type>>(std::get<std::weak_ptr<target_type>>(other._holder));
        }
        else if (std::holds_alternative<std::unique_ptr<target_type>>(other._holder)) {
          if (transfer) {
            _holder.swap(other._holder);
            other._holder.emplace<std::unique_ptr<target_type>>(std::get<std::unique_ptr<target_type>>(_holder).get());
          }
          else {
            _holder.emplace<std::unique_ptr<target_type>>(std::get<std::unique_ptr<target_type>>(other._holder).get());
          }
        }
        else {
          _holder.emplace<std::shared_ptr<target_type>>(std::get<std::shared_ptr<target_type>>(_holder));
        }
      }

      any_ptr(std::unique_ptr<target_type> ptr) : _holder(std::move(ptr)) {}
      any_ptr(std::shared_ptr<target_type> ptr) : _holder(std::move(ptr)) {}
      any_ptr(std::weak_ptr<target_type> ptr) : _holder(std::move(ptr)) {}

      locked_any_ptr<target_type> lock() {
        if (std::holds_alternative<target_type *>(_holder)) {
          return locked_any_ptr<target_type>(std::get<target_type *>(_holder));
        }
        else if (std::holds_alternative<std::weak_ptr<target_type>>(_holder)) {
          return locked_any_ptr<target_type>(std::get<std::weak_ptr<target_type>>(_holder).lock());
        }
        else if (std::holds_alternative<std::unique_ptr<target_type>>(_holder)) {
          return locked_any_ptr<target_type>(std::get<std::unique_ptr<target_type>>(_holder).get());
        }
        else {
          return locked_any_ptr<target_type>(std::get<std::shared_ptr<target_type>>(_holder));
        }
      }
    private:
      std::variant<
        target_type *,
        std::unique_ptr<target_type>,
        std::shared_ptr<target_type>,
        std::weak_ptr<target_type>> _holder;
    };
  }
}