/*
Copyright (c) 2022, Vadim Malyshev, lboss75@gmail.com
All rights reserved
*/
#pragma once

#include "vlib_task.h"
#include "vlib_cancellation_token.h"
#include "vlib_any_ptr.h"

namespace vlib {
  namespace core {
    class input_stream {
    public:
      virtual ~input_stream();
      virtual task<size_t> read(void * buffer, size_t size, core::cancellation_token token) = 0;
      task<std::string> read_line(size_t max_len, core::cancellation_token token);
    };
    class output_stream {
    public:
      virtual ~output_stream();
      virtual task<void> write_all(const void* buffer, size_t size, core::cancellation_token token)
      {
        for (;;) {
          auto written = co_await write(buffer, size, token);
          if (written == size) {
            co_return;
          }
          buffer = (const uint8_t*)buffer + written;
          size -= written;
        }
      }
      virtual task<size_t> write(const void* buffer, size_t size, core::cancellation_token token) = 0;
      virtual task<void> close() = 0;
    };

    class input_stream_with_buffer : public input_stream {
    public:
      input_stream_with_buffer(any_ptr<input_stream> target_stream, size_t buffer_size = 1024);

      task<size_t> read(void* buffer, size_t size, core::cancellation_token token) override;

    private:
      std::unique_ptr<char[]> _buffer;
      size_t _offset;
      size_t _readed;
      size_t _buffer_size;
      bool _eof;
      any_ptr<input_stream> _target_stream;
    };

    class input_stream_with_limit : public input_stream {
    public:
      input_stream_with_limit(any_ptr<input_stream> target_stream, size_t limit);

      task<size_t> read(void* buffer, size_t size, core::cancellation_token token) override;

    private:
      size_t _left;
      any_ptr<input_stream> _target_stream;
    };
  }
}
