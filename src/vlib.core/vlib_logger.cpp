#include <iostream>
#include "vlib_logger.h"

vlib::core::log_provider::~log_provider()
{
}

vlib::core::logger::logger(std::shared_ptr<service_provider> sp, std::string category)
  : _category(std::move(category)), _providers(sp->get_services<log_provider>()) {
}

vlib::core::logger::~logger()
{
}

vlib::core::task<void> vlib::core::logger::log(log_level level, std::string message) const {
  for (auto& provider : _providers) {
    co_await provider->log(_category, level, message);
  }
  co_return;
}

vlib::core::service_collection& vlib::core::console_log_provider::use_console(service_collection& services)
{
  return services.add_singleton<log_provider>([](service_provider & sp) {
    return std::make_shared<console_log_provider>();
    });
}

vlib::core::task<void> vlib::core::console_log_provider::log(std::string_view category, log_level level, std::string_view message)
{
  switch (level) {
  case log_level::trace:
    std::clog << category << ": trace: " << message << "\n";
    break;

  case log_level::debug:
    std::clog << category << ": debug: " << message << "\n";
    break;

  case log_level::information:
    std::clog << category << ": info: " << message << "\n";
    break;

  case log_level::warning:
    std::clog << category << ": warn: " << message << "\n";
    break;

  case log_level::error:
    std::clog << category << ": error: " << message << "\n";
    break;

  default:
    std::clog << category << ": level" << (int)level << ": " << message << "\n";
    break;
  }

  return task<void>::completed();
}
