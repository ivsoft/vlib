/*
Copyright (c) 2022, Vadim Malyshev, lboss75@gmail.com
All rights reserved
*/

#pragma once

#include <list>
#include <string>
#include <string_view>
#include <sstream>
#include <cctype>
#include <locale>

#ifdef max
#undef max
#endif

#ifdef min
#undef min
#endif

namespace vlib {
  namespace core {
    static inline void replace_string(std::string& result, const std::string_view& original, const std::string_view& target) {
      size_t index = 0;
      for (;;) {
        index = result.find(original, index);
        if (index == std::string::npos) {
          break;
        }
        result.replace(index, original.length(), target);
        index += target.length();
      }
    }

    template <typename string_type>
    static inline std::string replace_string(const string_type& str, const std::string_view& original, const std::string_view& target) {
      std::string result { str };
      replace_string(result, original, target);
      return result;
    }

    template <typename string_type>
    static inline std::string display_string(const string_type& str, size_t first_symbols = 10, size_t last_symbols = 10) {
      if (str.length() < first_symbols + last_symbols + 3) {
        return std::string{ str };
      }

      std::stringstream result;
      result << str.substr(0, first_symbols) << "..." << str.substr(str.length() - last_symbols - 1, last_symbols);
      return result.str();
    }

    template <typename string_type>
    static inline std::string_view trim_left(const string_type & data, std::string_view trimChars) {
      std::string_view sv{ data };
      sv.remove_prefix(std::min(sv.find_first_not_of(trimChars), sv.size()));
      return sv;
    }

    template <typename string_type>
    static inline std::string_view trim_left(const string_type& data) {
      std::string_view sv{ data };
      auto it = std::find_if(sv.begin(), sv.end(), [](char ch) { return !std::isspace<char>(ch, std::locale::classic()); });
      return sv.substr(it - sv.begin());
    }

    template <typename string_type>
    static inline std::string_view trim_right(const string_type& data, std::string_view trimChars) {
      std::string_view sv{ data };
      return sv.substr(0, sv.find_last_not_of(trimChars) + 1);
    }

    template <typename string_type>
    static inline std::string_view trim_right(const string_type& data) {
      std::string_view sv{ data };
      auto it = std::find_if(sv.rbegin(), sv.rend(), [](char ch) { return !std::isspace<char>(ch, std::locale::classic()); });
      return sv.substr(0, it.base() - sv.begin());
    }
    template <typename string_type>
    static inline std::string_view trim(const string_type& data, std::string_view trimChars) {
      return trim_left(trim_right(data, trimChars), trimChars);
    }
    template <typename string_type>
    static inline std::string_view trim(const string_type& data) {
      return trim_left(trim_right(data));
    }

    static inline std::list<std::string_view> split_string(const std::string_view& s, std::string_view trimChars, const size_t max_count = std::numeric_limits<size_t>::max()) {
      std::list<std::string_view> result;
      std::string::size_type start = 0;
      for (;;) {
        if (max_count <= result.size() + 1) {
          auto value = s.substr(start);
          result.push_back(value);
          break;
        }
        auto p = s.find_first_of(trimChars, start);
        if (std::string::npos == p) {
          auto value = s.substr(start);
          result.push_back(value);
          break;
        }
        else {
          auto value = s.substr(start, p - start);
          start = p + 1;
          result.push_back(value);
        }
      }

      return result;
    }
    static inline std::list<std::string_view> split_string(const std::string_view& s, const size_t max_count = std::numeric_limits<size_t>::max()) {
      std::list<std::string_view> result;
      auto start = s.begin();
      for (;;) {
        if (max_count <= result.size() + 1) {
          auto value = s.substr(start - s.begin());
          result.push_back(value);
          break;
        }
        auto p = std::find_if(start, s.end(), [](char ch) { return std::isspace<char>(ch, std::locale::classic()); });
        if (s.end() == p) {
          auto value = s.substr(start - s.begin());
          result.push_back(value);
          break;
        }
        else {
          auto value = s.substr(start - s.begin(), p - start);
          start = p;
          ++start;
          result.push_back(value);
        }
      }

      return result;
    }

    static inline void str_replace(std::string& result, char original, char target) {
      size_t index = 0;
      for (;;) {
        index = result.find(original, index);
        if (index == std::string::npos) {
          break;
        }
        result[index] = target;
        ++index;
      }
    }

    static inline bool str_equal_ignore_case(const std::string_view& a, const std::string_view& b) {
      return std::equal(
        a.begin(), a.end(),
        b.begin(), b.end(),
        [](char ch_a, char ch_b) {
          return tolower(ch_a) == tolower(ch_b);
        });
    }
  }
}