#pragma once

#include <list>
#include <unordered_map>

#include "vlib_task.h"
#include "vlib_cancellation_token_source.h"

namespace vlib {
  namespace core {
    class cancellation_token {
    public:
      cancellation_token() {}
      cancellation_token(const cancellation_token & original) noexcept : _source(original._source) {}
      cancellation_token(cancellation_token && original)  noexcept : _source(std::move(original._source)) {}
      cancellation_token(std::shared_ptr<cancellation_token_source> source)  noexcept : _source(std::move(source)) {}

      cancellation_token& operator = (const cancellation_token& original)  noexcept { _source = original._source; return *this; }
      cancellation_token& operator = (cancellation_token&& original)  noexcept { _source = std::move(original._source); return *this; }

      bool is_cancelled() const noexcept {
        if (!_source) {
          return false;
        }
        else {
          return _source->is_cancelled();
        }
      }


      struct handler_ref {
        int token;
        std::shared_ptr<cancellation_token_source> source;

        void remove() {
          if (source) {
            source->remove(token);
          }
        }
      };

      handler_ref on_cancel(moveonly_function_noexcept<task<void>(void)> cancel_handler) {
        if (!_source) {
          return handler_ref{ 0 };
        }
        else {
          return handler_ref{ _source->on_cancel(std::move(cancel_handler)), _source };
        }
      }

    private:
      std::shared_ptr<cancellation_token_source> _source;
    };
  }
}