#include "vlib_task.h"

vlib::core::task_pool* vlib::core::task_pool::shared_task_pool = nullptr;

vlib::core::task_pool::task_pool() noexcept
  : _thread_pool((std::size_t)std::thread::hardware_concurrency() << 1)
{
}

void vlib::core::task_pool::add(moveonly_function_noexcept<void(void)> callback) noexcept {
  _thread_pool.add(std::move(callback));
}
