#pragma once

#include <vector>
#include <queue>
#include <condition_variable>
#include <thread>
#include <atomic>
#include "vlib_moveonly_function.h"

namespace vlib {
  namespace core {
    class thread_pool {
    public:
      thread_pool(std::size_t capacity) noexcept;
      ~thread_pool() noexcept { stop(); }

      void add(moveonly_function<void(void)> callback) noexcept;

      void stop() noexcept;
      void wait_all() noexcept;

    private:
      std::atomic<bool> _quite;
      std::vector<std::thread> _threads;
      int _task_in_list;

      std::queue<moveonly_function<void(void)>> _task_list;
      std::mutex _task_list_mutex;
      std::condition_variable _task_list_cv;
      std::condition_variable _task_list_empty_cv;

      void work_func() noexcept;
    };

    class thread_pool_noexcept {
    public:
      thread_pool_noexcept(std::size_t capacity) noexcept;
      ~thread_pool_noexcept() noexcept { stop(); }

      void add(moveonly_function_noexcept<void(void)> callback) noexcept;

      void stop() noexcept;
      void wait_all() noexcept;

    private:
      std::atomic<bool> _quite;
      std::vector<std::thread> _threads;
      int _task_in_list;

      std::queue<moveonly_function_noexcept<void(void)>> _task_list;
      std::mutex _task_list_mutex;
      std::condition_variable _task_list_cv;
      std::condition_variable _task_list_empty_cv;

      void work_func() noexcept;
    };
  }
}