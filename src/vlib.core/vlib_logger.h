/*
Copyright (c) 2022, Vadim Malyshev, lboss75@gmail.com
All rights reserved
*/
#pragma once

#include <string_view>

#include "vlib_task.h"
#include "vlib_service_provider.h"

namespace vlib {
  namespace core {
    enum class log_level {
      trace,
      debug,
      information,
      warning,
      error
    };

    class log_provider {
    public:
      virtual ~log_provider();
      virtual task<void> log(std::string_view category, log_level level, std::string_view message) = 0;
    };

    class console_log_provider : public log_provider {
    public:
      static service_collection& use_console(service_collection& services);

      task<void> log(std::string_view category, log_level level, std::string_view message) override;
    };

    class logger {
    public:
      logger(std::shared_ptr<service_provider> sp, std::string category);
      ~logger();

      task<void> log(log_level level, std::string message) const;
    private:
      std::string _category;
      std::list<std::shared_ptr<log_provider>> _providers;
    };
  }
}
