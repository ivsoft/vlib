#pragma once

#include <condition_variable>
#include <mutex>

namespace vlib {
  namespace core {
    template <typename state_t = int>
    class state_machine {
    private:
      std::condition_variable _cv;
      std::mutex _mutex;
      state_t _state;
    public:
      state_machine() : _state() {}

      template<std::convertible_to<state_t> U> // C++20 concept
      void wait(const state_t & target_state, U && new_state) {
        for (;;) {
          std::unique_lock<std::mutex> lock(_mutex);

          if (_state == target_state) {
            _state = std::forward<U>(new_state);
            _cv.notify_all();
            return;
          }

          _cv.wait(lock, [this, &target_state]() { return _state == target_state; });
        }
      }
      void wait(const state_t & target_state) {
        for (;;) {
          std::unique_lock<std::mutex> lock(_mutex);

          if (_state == target_state) {
            return;
          }

          _cv.wait(lock, [this, &target_state]() { return _state == target_state; });
        }
      }
    };
  }
}