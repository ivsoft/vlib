#pragma once

namespace vlib {
  namespace core {
    template<typename result_type>
    class async_enumerable {
    private:
      struct enumerable_result {
        task<bool>::promise_type _next_promise;
        task<void>::promise_type _promise;
        result_type _value;

        enumerable_result() : _value() {
        }

        template<std::convertible_to<result_type> From> // C++20 concept
        task<void> emplace(From&& from) noexcept {
          co_await _promise.get_return_object();

          _promise.reset();

          _value = std::move(from);
          _next_promise.return_value(true);
        }
        task<void> done() noexcept {
          co_await _promise.get_return_object();

          _next_promise.return_value(false);
        }
        task<void> set_exception(std::exception_ptr ex) noexcept {
          co_await _promise.get_return_object();

          _next_promise.set_exception(ex);
        }

        task<bool> next() noexcept {
          _promise.return_void();

          auto result = co_await _next_promise.get_return_object();

          if (result) {
            _next_promise.reset();
          }
          co_return result;
        }

        result_type value() const noexcept {
          return _value;
        }

      };

      class future {
      public:
        future(std::shared_ptr<enumerable_result> result) noexcept
          : _result(std::move(result)) {
        }

        future(future&&)  noexcept = default;
        future& operator =(future&&)  noexcept = default;

        task<bool> next() noexcept {
          return _result->next();
        }

        result_type value() const noexcept {
          return _result->value();
        }

      private:
        std::shared_ptr<enumerable_result> _result;
      };

      class promise {
      public:
        promise()
        {
          _result = std::make_shared<enumerable_result>();
        }

        future get_future()  noexcept {
          return future(_result);
        }

        template<std::convertible_to<result_type> From> // C++20 concept
        task<void> emplace(From&& from) {
          return _result->emplace(std::move(from));

        }

        void set_exception(std::exception_ptr ex) noexcept {
          _result->set_exception(ex);
        }

        task<void> done() noexcept {
          return _result->done();
        }


      private:
        std::shared_ptr<enumerable_result> _result;
      };

      future _f;

    public:
      class promise_type {
      private:
        promise _p;
      public:
        async_enumerable get_return_object() noexcept { return async_enumerable(_p.get_future()); }
        task<void> return_void() noexcept { return _p.done(); }

        std::suspend_never initial_suspend() noexcept { return {}; }
        std::suspend_never final_suspend() noexcept { return {}; }

        template<std::convertible_to<result_type> From> // C++20 concept
        task<void> yield_value(From&& from) {
          return _p.emplace(std::forward<From>(from));
        }
        void unhandled_exception() noexcept {
          _p.set_exception(std::current_exception());
        }
      };

      async_enumerable(future f) noexcept : _f(std::move(f)) {}

      task<bool> next() noexcept {
        return _f.next();
      }

      result_type value() const noexcept {
        return _f.value();
      }

      //bool await_ready() noexcept {
      //  return false;
      //}

      //auto await_resume() noexcept {
      //  return 1;
      //}

    };
  }
}