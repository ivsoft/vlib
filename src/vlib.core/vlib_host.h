/*
Copyright (c) 2022, Vadim Malyshev, lboss75@gmail.com
All rights reserved
*/
#pragma once

#include "vlib_service_provider.h"
#include "vlib_cancellation_token.h"

namespace vlib {
  namespace core {
    class host_lifetime {
    public:
      virtual ~host_lifetime();

      virtual task<void> wait();
      void stop();
    private:
      cancellation_token_source _source;
    };

    class hosted_service {
    public:
      virtual ~hosted_service();
      virtual task<void> start(cancellation_token token) = 0;
      virtual task<void> stop(cancellation_token token) = 0;
    };

    class host {
    public:
      host(std::shared_ptr<service_provider> sp);

      std::shared_ptr<service_provider> services() { return _sp; }

      task<void> start(cancellation_token token);
      task<void> stop(cancellation_token token);

      task<void> run(cancellation_token token);

    private:
      std::shared_ptr<service_provider> _sp;
    };

    class host_builder {
    public:

      service_collection& services() { return _services; }

      host build();

    private:
      service_collection _services;
    };

  }
}