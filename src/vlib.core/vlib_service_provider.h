#pragma once
#include <memory>
#include <mutex>
#include <unordered_map>
#include <list>
#include "vlib_moveonly_function.h"

namespace vlib {
  namespace core {
    class service_provider;//Forward


    class service_description {
    public:
      using type_info_id = std::reference_wrapper<const std::type_info>;

      struct Hasher {
        std::size_t operator()(type_info_id code) const
        {
          return code.get().hash_code();
        }
      };

      struct EqualTo {
        bool operator()(type_info_id x, type_info_id y) const
        {
          return x.get() == y.get();
        }
      };

      using service_map = std::unordered_map<type_info_id, std::list<std::unique_ptr<service_description>>, Hasher, EqualTo>;

      virtual ~service_description() noexcept;
      virtual void get_scope_service(service_map& scope_services) = 0;


    };

    template <typename interface_type>
    class typed_service_description : public service_description {
    public:
      virtual std::shared_ptr<interface_type> get(service_provider& sp) = 0;
    };

    template <typename interface_type>
    class transient_service_description : public typed_service_description<interface_type> {
    public:
      transient_service_description(moveonly_function<std::shared_ptr<interface_type>(service_provider& sp)> && factory)
      : _factory(std::move(factory)) {
      }

      std::shared_ptr<interface_type> get(service_provider& sp) override {
        return _factory(sp);
      }

      void get_scope_service(service_description::service_map& scope_services)  override {}
    private:
      moveonly_function<std::shared_ptr<interface_type>(service_provider& sp)> _factory;
    };

    template <typename interface_type>
    class singleton_service_description : public typed_service_description<interface_type> {
    public:
      singleton_service_description(moveonly_function<std::shared_ptr<interface_type>(service_provider& sp)> && factory)
        : _factory(std::move(factory)) {
      }

      std::shared_ptr<interface_type> get(service_provider& sp) override {
        std::unique_lock<std::mutex> lock(_instanceMutex);
        if (!_instance) {
          _instance = _factory(sp);
        }
        return _instance;
      }
      void get_scope_service(service_description::service_map& scope_services)  override {}

    private:
      std::mutex _instanceMutex;
      std::shared_ptr<interface_type> _instance;
      moveonly_function<std::shared_ptr<interface_type>(service_provider& sp)> _factory;
    };

    template <typename interface_type>
    class scoped_service_description : public typed_service_description<interface_type> {
    private:
      std::mutex _instanceMutex;
      std::shared_ptr<interface_type> _instance;

      struct FactoryHolder {
        moveonly_function<std::shared_ptr<interface_type>(service_provider& sp)> _factory;

        FactoryHolder(moveonly_function<std::shared_ptr<interface_type>(service_provider& sp)>&& factory)
          : _factory(std::move(factory)) {
        }
      };
      std::shared_ptr<FactoryHolder> _factory;

    public:
      scoped_service_description(moveonly_function<std::shared_ptr<interface_type>(service_provider& sp)>&& factory)
        : _factory(std::make_shared<FactoryHolder>(std::move(factory))) {
      }
      scoped_service_description(std::shared_ptr<FactoryHolder> factory)
        : _factory(factory) {
      }

      std::shared_ptr<interface_type> get(service_provider& sp) override {
        std::unique_lock<std::mutex> lock(_instanceMutex);
        if (!_instance) {
          _instance = _factory->_factory(sp);
        }
        return _instance;
      }

      void get_scope_service(service_description::service_map& scope_services) override {
        scope_services[typeid(interface_type)].emplace_back(std::make_unique<scoped_service_description<interface_type>>(_factory));
      }
    };

    class service_provider : public std::enable_shared_from_this<service_provider> {
    public:
      service_provider(std::shared_ptr<service_provider> parent, service_description::service_map&& services);
      service_provider(service_description::service_map && services);

      service_provider() = delete;
      service_provider(const service_provider &) = delete;
      service_provider(service_provider &&) = default;

      service_provider & operator = (const service_provider&) = delete;
      service_provider & operator = (service_provider && ) = delete;

      template <typename interface_type>
      std::shared_ptr<interface_type> get() {
        auto p = _services.find(typeid(interface_type));
        if (_services.end() == p || p->second.empty()) {
          if (!_parent) {
            return std::shared_ptr<interface_type>();
          }
          else {
            return _parent->get<interface_type>();
          }
        }

        return static_cast<typed_service_description<interface_type> *>(p->second.begin()->get())->get(*this);
      }

      template <typename interface_type>
      std::shared_ptr<interface_type> get_required() {
        auto result = get<interface_type>();
        if (!result) {
          throw std::runtime_error(std::string("interface_type type ") + typeid(interface_type).name() + " can't be resolved");
        }
        return result;           
      }

      template <typename interface_type>
      std::list<std::shared_ptr<interface_type>> get_services() {
        auto p = _services.find(typeid(interface_type));
        if (_services.end() == p) {
          if (!_parent) {
            return std::list<std::shared_ptr<interface_type>>();
          }
          else {
            return _parent->get_services<interface_type>();
          }
        }
        else {
          std::list<std::shared_ptr<interface_type>> result;
          if (_parent) {
            result = _parent->get_services<interface_type>();
          }
          for (const auto& factory : p->second) {
            result.emplace_back(static_cast<typed_service_description<interface_type> *>(factory.get())->get(*this));
          }
          return result;
        }
      }

      std::shared_ptr<service_provider> create_scope();

    private:
      const std::shared_ptr<service_provider> _parent;
      const service_description::service_map _services;
    };

    class service_collection {
    public:
      template <typename interface_type>
      service_collection & add_transient(moveonly_function<std::shared_ptr<interface_type>(service_provider& sp)> && factory) {
        _services[typeid(interface_type)].emplace_back(std::make_unique<transient_service_description<interface_type>>(std::move(factory)));
        return *this;
      }

      template <typename interface_type>
      service_collection & add_singleton(moveonly_function<std::shared_ptr<interface_type>(service_provider& sp)> && factory) {
        _services[typeid(interface_type)].emplace_back(std::make_unique<singleton_service_description<interface_type>>(std::move(factory)));
        return *this;
      }

      template <typename interface_type>
      service_collection & add_scoped(moveonly_function<std::shared_ptr<interface_type>(service_provider& sp)>&& factory) {
        _services[typeid(interface_type)].emplace_back(std::make_unique<scoped_service_description<interface_type>>(std::move(factory)));
        return *this;
      }

      template <typename interface_type>
      bool has() const {
        const auto p = _services.find(typeid(interface_type));
        if (_services.end() == p) {
          return false;
        }
        return !p->second.empty();
      }

      std::shared_ptr<service_provider> build();

    private:
      service_description::service_map _services;
    };
  }
}