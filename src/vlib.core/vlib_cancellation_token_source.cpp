#include "vlib_cancellation_token_source.h"
#include "vlib_cancelled_error.h"

vlib::core::task<void> vlib::core::cancellation_token_source::cancel() noexcept {
  _is_cancel = true;

  for (bool bContinue = true; bContinue;) {
    bContinue = false;
    std::unique_lock<std::mutex> lock(_mutex);

    for (auto& handler : _handlers) {
      if (!handler.second._is_raised) {
        handler.second._is_raised = true;
        lock.unlock();

        co_await handler.second._target();

        bContinue = true;
        break;
      }
    }
  }
}

int vlib::core::cancellation_token_source::on_cancel(moveonly_function_noexcept<task<void>(void)> cancel_handler) {
  std::unique_lock<std::mutex> lock(_mutex);
  if (_is_cancel) {
    lock.unlock();

    throw cancelled_error();
  }
  else {
    int token = (int)_handlers.size();
    while (_handlers.end() != _handlers.find(token)) {
      ++token;
    }
    _handlers.emplace(token, handler_info { std::move(cancel_handler), false });
    return token;
  }
}

void vlib::core::cancellation_token_source::remove(int token) noexcept {
  std::lock_guard<std::mutex> lock(_mutex);
  _handlers.erase(token);
}
