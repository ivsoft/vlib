#include <cstring>
#include "vlib_stream.h"

vlib::core::input_stream::~input_stream()
{
}

vlib::core::task<std::string> vlib::core::input_stream::read_line(size_t max_len, core::cancellation_token token) {
  std::string result;
  char ch;
  while (0 != co_await read(&ch, 1, token)) {
    if ('\n' == ch) {
      break;
    }
    result += ch;
    if (result.size() > max_len) {
      throw std::runtime_error("Very long string");
    }
  }
  co_return result;
}

vlib::core::output_stream::~output_stream()
{
}

vlib::core::input_stream_with_buffer::input_stream_with_buffer(
  any_ptr<input_stream> target_stream,
  size_t buffer_size)
  : _buffer(std::make_unique<char[]>(buffer_size)),
  _offset(0), _readed(0), _buffer_size(buffer_size), _eof(false),
  _target_stream(std::move(target_stream)) {
}

vlib::core::task<size_t> vlib::core::input_stream_with_buffer::read(void* buffer, size_t size, core::cancellation_token token)
{
  for (;;) {
    if (_readed > _offset) {
      const auto result = std::min(size, _readed - _offset);
      memcpy(buffer, _buffer.get() + _offset, result);
      _offset += result;
      co_return result;
    }
    if (_eof) {
      co_return 0;
    }
    auto target_stream = _target_stream.lock();
    if (size >= _buffer_size) {
      const auto result = co_await target_stream->read(buffer, size, std::move(token));
      if (0 == result) {
        _eof = true;
      }
      co_return result;
    }
    else {
      _offset = 0;
      _readed = co_await target_stream->read(_buffer.get(), _buffer_size, std::move(token));
      if (0 == _readed) {
        _eof = true;
        co_return 0;
      }
    }
  }
}

vlib::core::input_stream_with_limit::input_stream_with_limit(
  any_ptr<input_stream> target_stream, size_t limit)
  : _left(limit), _target_stream(std::move(target_stream)) {
}

vlib::core::task<size_t> vlib::core::input_stream_with_limit::read(
  void* buffer,
  size_t size,
  core::cancellation_token token) {
  if (_left > 0) {
    auto target_stream = _target_stream.lock();
    const auto readed = co_await target_stream->read(buffer, std::min(size, _left), std::move(token));
    _left -= readed;
    co_return readed;
  }
  else {
    co_return 0;
  }
}