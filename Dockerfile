FROM ubuntu:22.04
RUN apt-get -qq update
RUN apt-get -qq install -y cmake clang git curl zip unzip tar bison pkg-config
RUN apt-get remove -y gcc
RUN apt autoremove -y
RUN git clone https://github.com/Microsoft/vcpkg.git
RUN ./vcpkg/bootstrap-vcpkg.sh
RUN ./vcpkg/vcpkg install gtest
ADD src src/
ADD test test/
ADD CMakeLists.txt .
RUN mkdir build

WORKDIR /build
RUN cmake -DCMAKE_TOOLCHAIN_FILE=/vcpkg/scripts/buildsystems/vcpkg.cmake ..
RUN make -j8
# Test
RUN ctest


