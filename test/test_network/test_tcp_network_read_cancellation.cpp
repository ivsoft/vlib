/*
Copyright (c) 2017, Vadim Malyshev, lboss75@gmail.com
All rights reserved
*/
#include <gtest/gtest.h>

#include "vlib_connection_pool.h"
#include "vlib_tcp_server.h"
#include "vlib_tcp_client.h"
#include "vlib_cancelled_error.h"
#include "vlib_state_machine.h"

#include "memory_leak_detector.h"

static vlib::core::task<void> copy_stream(
  vlib::core::input_stream& reader,
  vlib::core::output_stream& writer,
  vlib::core::cancellation_token token) {
  uint8_t buffer[1024];
  for (;;) {
    size_t readed = co_await reader.read(buffer, 1024, token);
    if (0 == readed) {
      break;
    }

    co_await writer.write_all(buffer, readed, token);
  }
}


TEST(network_tests, test_server_with_read_cancellation)
{
  memory_leak_detector detector;
  uint8_t sent_data[10000];
  uint8_t received_data[10000];
  size_t total_received = 0;
  bool cancelled_error_at_read = false;

  {
    vlib::core::state_machine state;

    vlib::core::task_pool::shared_task_pool = new vlib::core::task_pool();

    auto cancellation_source = std::make_shared<vlib::core::cancellation_token_source>();
    auto read_cancellation_source = std::make_shared<vlib::core::cancellation_token_source>();
   
    vlib::network::connection_pool pool;
    pool.start(100);

    class server_handler : public vlib::network::tcp_server_client {
    public:
      vlib::core::task<void> new_connection(vlib::network::connection& c, vlib::core::cancellation_token token) noexcept override {
        co_await copy_stream(c.input_stream(), c.output_stream(), std::move(token));
      }
    } server_client;

    vlib::network::tcp_server server(pool);
    server.start(vlib::network::network_address::any_ip4(8000), server_client);


    for (int i = 0; i < sizeof(sent_data) / sizeof(sent_data[0]); ++i) {
      sent_data[i] = std::rand();
    }

    vlib::network::tcp_client client(pool);

    auto address = vlib::network::network_address::tcp_ip4("localhost", 8000);
    auto& s = client.connect(address);

    std::thread t1([&s, &sent_data, &state, cancellation_source]() {
      s.output_stream().write_all(sent_data, sizeof(sent_data) / sizeof(sent_data[0]) / 2, vlib::core::cancellation_token(cancellation_source)).get();
      state.wait(0, 1);
      state.wait(3);
      s.output_stream().write_all(sent_data, sizeof(sent_data) / sizeof(sent_data[0]) / 2, vlib::core::cancellation_token(cancellation_source)).get();
      s.output_stream().close().get();
      });
    std::thread t2([&s, &received_data, &state, read_cancellation_source, &total_received, &cancelled_error_at_read]() {
      auto pos = received_data;
      const auto final = received_data + sizeof(received_data) / sizeof(received_data[0]);
      while (final > pos) {
        try {
          auto readed = s.input_stream().read(pos, final - pos, vlib::core::cancellation_token(read_cancellation_source)).get();
          if (0 == readed) {
            break;
          }
          total_received += readed;
          pos += readed;
        }
        catch (vlib::core::cancelled_error) {
          state.wait(2, 3);
          cancelled_error_at_read = true;
          break;
        }
      }
      });

    state.wait(1);
    std::this_thread::sleep_for(std::chrono::seconds(1));
    read_cancellation_source->cancel();
    state.wait(1, 2);

    t1.join();
    t2.join();

    client.close(s);
    server.stop();
    pool.stop();
    vlib::core::task_pool::shared_task_pool->wait_all();
    delete vlib::core::task_pool::shared_task_pool;
  }

  GTEST_ASSERT_EQ(true, cancelled_error_at_read);
  GTEST_ASSERT_EQ(total_received, sizeof(sent_data) / sizeof(sent_data[0]) / 2);
  for (int i = 0; i < sizeof(sent_data) / sizeof(sent_data[0]) / 2; ++i) {
    GTEST_ASSERT_EQ(sent_data[i], received_data[i]) << " at " << i;
  }
}

