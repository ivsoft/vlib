add_executable(
    test_network
    "test_tcp_network.cpp"
    "main.cpp"
    "test_tcp_network_close.cpp"
    "test_tcp_network_write_cancellation.cpp"
    "test_tcp_network_read_cancellation.cpp"
)
set_property(TARGET test_network PROPERTY CXX_STANDARD 20)

target_include_directories(
test_network
PRIVATE
    ${GTEST_INCLUDE_DIRS}
)

target_link_libraries(
test_network
vlib.core
vlib.network
test_commonlib
GTest::gtest_main
Threads::Threads
)

include(GoogleTest)
gtest_discover_tests(test_network)