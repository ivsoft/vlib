#include <gtest/gtest.h>
#include "vlib_network_runtime.h"

int main(int argc, char** argv) {
  std::srand(unsigned(std::time(0)));

  vlib::network::runtime network_runtime;

  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
