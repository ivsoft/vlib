/*
Copyright (c) 2017, Vadim Malyshev, lboss75@gmail.com
All rights reserved
*/
#include <gtest/gtest.h>
#include "vlib_connection_pool.h"
#include "vlib_tcp_server.h"
#include "vlib_tcp_client.h"
#include "memory_leak_detector.h"

static vlib::core::task<void> copy_stream(
  vlib::core::input_stream& reader,
  vlib::core::output_stream& writer,
  vlib::core::cancellation_token token) {
  uint8_t buffer[1024];
  for (;;) {
    size_t readed = co_await reader.read(buffer, 1024, token);
    if (0 == readed) {
      break;
    }

    co_await writer.write_all(buffer, readed, token);
  }
}

TEST(network_tests, test_server)
{
  memory_leak_detector detector;
  uint8_t sent_data[10000];
  uint8_t received_data[10000];

  {
    vlib::core::task_pool::shared_task_pool = new vlib::core::task_pool();

    auto cancellation_source = std::make_shared<vlib::core::cancellation_token_source>();

    vlib::network::connection_pool pool;
    pool.start(100);

    class server_handler : public vlib::network::tcp_server_client {
    public:
      vlib::core::task<void> new_connection(vlib::network::connection& c, vlib::core::cancellation_token token) noexcept override {
        co_await copy_stream(c.input_stream(), c.output_stream(), std::move(token));
      }
    } server_client;

    vlib::network::tcp_server server(pool);
    server.start(vlib::network::network_address::any_ip4(8000), server_client);


    for (int i = 0; i < sizeof(sent_data) / sizeof(sent_data[0]); ++i) {
      sent_data[i] = std::rand();
    }

    vlib::network::tcp_client client(pool);

    auto address = vlib::network::network_address::tcp_ip4("localhost", 8000);
    auto& s = client.connect(address);

    std::thread t1([&s, &sent_data, cancellation_source]() {
      s.output_stream().write_all(sent_data, sizeof(sent_data) / sizeof(sent_data[0]), vlib::core::cancellation_token(cancellation_source)).get();
      s.output_stream().close().get();
      });

    std::thread t2([&s, &received_data, cancellation_source]() {
      auto pos = received_data;
      const auto final = received_data + sizeof(received_data) / sizeof(received_data[0]);
      while (final > pos) {
        auto readed = s.input_stream().read(pos, final - pos, vlib::core::cancellation_token(cancellation_source)).get();
        if (0 == readed) {
          break;
        }
        pos += readed;
      }
      });

    t1.join();
    t2.join();

    client.close(s);
    server.stop();
    pool.stop();
    vlib::core::task_pool::shared_task_pool->wait_all();
    delete vlib::core::task_pool::shared_task_pool;
  }

  for (int i = 0; i < sizeof(sent_data) / sizeof(sent_data[0]); ++i) {
    GTEST_ASSERT_EQ(sent_data[i], received_data[i]) << " at " << i;
  }
}

TEST(network_tests, test_server_multi_client)
{
  memory_leak_detector detector;
  uint8_t sent_data[10000];

  {
    struct client_info {
      vlib::network::tcp_client client;
      std::thread t1;
      std::thread t2;
      vlib::network::connection* connection;
      uint8_t received_data[10000];

      client_info(vlib::network::connection_pool& pool)
        : client(pool), connection(nullptr) {
      }
    };
    std::list<client_info> clients;

    vlib::core::task_pool::shared_task_pool = new vlib::core::task_pool();

    auto cancellation_source = std::make_shared<vlib::core::cancellation_token_source>();

    vlib::network::connection_pool pool;
    pool.start(100);

    class server_handler : public vlib::network::tcp_server_client {
    public:
      vlib::core::task<void> new_connection(vlib::network::connection& c, vlib::core::cancellation_token token) noexcept override {
        co_await copy_stream(c.input_stream(), c.output_stream(), std::move(token));
      }
    } server_client;

    vlib::network::tcp_server server(pool);
    server.start(vlib::network::network_address::any_ip4(8000), server_client);


    for (int i = 0; i < sizeof(sent_data) / sizeof(sent_data[0]); ++i) {
      sent_data[i] = std::rand();
    }

    for (int client_id = 0; client_id < 100; ++client_id) {
      auto& info = clients.emplace_back(pool);

      auto address = vlib::network::network_address::tcp_ip4("localhost", 8000);
      info.connection = &info.client.connect(address);

      info.t1 = std::thread([s = info.connection, &sent_data, cancellation_source]() {
        s->output_stream().write_all(sent_data, sizeof(sent_data) / sizeof(sent_data[0]), vlib::core::cancellation_token(cancellation_source)).get();
        s->output_stream().close().get();
      });

      info.t2 = std::thread([s = info.connection, received_data = info.received_data, cancellation_source]() {
        auto pos = received_data;
        const auto final = received_data + 10000;
        while (final > pos) {
          auto readed = s->input_stream().read(pos, final - pos, vlib::core::cancellation_token(cancellation_source)).get();
          if (0 == readed) {
            break;
          }
          pos += readed;
        }
      });
    }
    for (auto& info : clients) {
      info.t1.join();
      info.t2.join();
    }
    for (auto& info : clients) {
      info.client.close(*info.connection);
    }

    server.stop();
    pool.stop();
    vlib::core::task_pool::shared_task_pool->wait_all();
    delete vlib::core::task_pool::shared_task_pool;

    int index = 0;
    for (auto& info : clients) {
      for (int i = 0; i < sizeof(sent_data) / sizeof(sent_data[0]); ++i) {
        GTEST_ASSERT_EQ(sent_data[i], info.received_data[i]) << " at " << i << "," << index;
      }
      ++index;
    }
    clients.clear();
  }
}
