
/*
Copyright (c) 2022, Vadim Malyshev, lboss75@gmail.com
All rights reserved
*/

#pragma once

#ifdef _WIN32
#include <crtdbg.h>

class memory_leak_detector {
public:
  memory_leak_detector();
  ~memory_leak_detector();

private:
  void report_failure(size_t unfreedBytes);

  _CrtMemState _memState;
};

#else

class memory_leak_detector {
public:
  memory_leak_detector();
  ~memory_leak_detector();
};

#endif