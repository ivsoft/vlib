#include "memory_leak_detector.h"

#ifdef _WIN32
#include <gtest/gtest.h>

memory_leak_detector::memory_leak_detector() {
  _CrtMemCheckpoint(&_memState);
}

memory_leak_detector:: ~memory_leak_detector() {
  _CrtMemState stateNow;
  _CrtMemCheckpoint(&stateNow);

  _CrtMemState stateDiff;
  const auto diffResult = _CrtMemDifference(&stateDiff, &_memState, &stateNow);
  if (diffResult) {
    report_failure(stateDiff.lSizes[1]);
  }
}

void memory_leak_detector::report_failure(size_t unfreedBytes) {
  GTEST_FAIL() << "Memory leak of " << unfreedBytes << " byte(s) detected.";
}
#else

memory_leak_detector::memory_leak_detector() {
}

memory_leak_detector:: ~memory_leak_detector() {
}

#endif