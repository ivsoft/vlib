add_library (test_commonlib STATIC
	"memory_leak_detector.h" "memory_leak_detector.cpp")
set_property(TARGET test_commonlib PROPERTY CXX_STANDARD 20)

target_include_directories(
  test_commonlib
	PUBLIC
		${CMAKE_CURRENT_SOURCE_DIR}
PRIVATE
    ${GTEST_INCLUDE_DIRS}
)

target_link_libraries(
  test_commonlib
  ${GTEST_LIBRARIES}
)