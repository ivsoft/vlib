/*
Copyright (c) 2022, Vadim Malyshev, lboss75@gmail.com
All rights reserved
*/
#include <gtest/gtest.h>
#include "vlib_host.h"
#include "vlib_tcp_server_host.h"
#include "vlib_http_server_host.h"
#include "vlib_http_client.h"
#include "vlib_http_uri.h"

#include "memory_leak_detector.h"

static void test_uri(
  std::string url,
  std::string_view schema,
  std::string_view server,
  std::string_view port,
  std::string_view path)
{
  
  vlib::http::uri test;
  GTEST_ASSERT_TRUE(test.try_parse(url));

  GTEST_ASSERT_EQ(schema, test.schema);
  GTEST_ASSERT_EQ(server, test.server);
  GTEST_ASSERT_EQ(port, test.port);
  GTEST_ASSERT_EQ(path, test.path);
}


TEST(http_tests, test_http_uri)
{
  test_uri("http://localhost:800/path/dsfs?query=wo&test", "http", "localhost", "800", "/path/dsfs?query=wo&test");
  test_uri("http://localhost/path/dsfs?query=wo&test", "http", "localhost", "", "/path/dsfs?query=wo&test");
  test_uri("http://localhost:800", "http", "localhost", "800", "");
  test_uri("http://localhost", "http", "localhost", "", "");
  test_uri("http://:800/path/dsfs?query=wo&test", "http", "", "800", "/path/dsfs?query=wo&test");

  test_uri("http://2001:0db8:85a3:0000:0000:8a2e:0370:7334:800/path/dsfs?query=wo&test", "http", "2001:0db8:85a3:0000:0000:8a2e:0370:7334", "800", "/path/dsfs?query=wo&test");
  test_uri("http://2001:0db8:85a3:0000:0000:8a2e:0370:7334/path/dsfs?query=wo&test", "http", "2001:0db8:85a3:0000:0000:8a2e:0370", "7334", "/path/dsfs?query=wo&test");
  test_uri("http://2001:0db8:85a3:0000:0000:8a2e:0370:7334:800", "http", "2001:0db8:85a3:0000:0000:8a2e:0370:7334", "800", "");
  test_uri("http://2001:0db8:85a3:0000:0000:8a2e:0370:7334", "http", "2001:0db8:85a3:0000:0000:8a2e:0370", "7334", "");
  //TODO: test_uri("http://[2001:0db8:85a3:0000:0000:8a2e:0370:7334]", "http", "[2001:0db8:85a3:0000:0000:8a2e:0370:7334]", "", "");
  test_uri("http://2001:0db8:85a3:0000:0000:8a2e:0370:7334:800/path/dsfs?query=wo&test", "http", "2001:0db8:85a3:0000:0000:8a2e:0370:7334", "800", "/path/dsfs?query=wo&test");
}
/*
TEST(http_tests, test_http_server)
{
  memory_leak_detector detector;

  std::string answer;

  {
    auto cancellation_source = std::make_shared<vlib::core::cancellation_token_source>();
    vlib::core::cancellation_token cancellation_token(cancellation_source);

    vlib::network::connection_pool pool;
    pool.start(100);

    vlib::core::host_builder builder;

    //vlib::network::tcp_server_host::bind(
    //  builder.services(),
    //  vlib::network::network_address::any_ip4(8000));

    //vlib::network::tcp_server_host::usePool(pool);

    auto host = builder.build();

    vlib::http::http_server_host::useRouting(
      host.services());

    vlib::http::http_server_host::mapUrl(
      "/",
      [](
        std::shared_ptr<vlib::core::service_provider>& service_provider,
        vlib::http::http_request request,
        vlib::http::http_response response,
        vlib::http::http_context context
        ) -> vlib::core::task<void> {
          co_await response.send("<html><body>Hello world</body></html>", context.cancellation_token());
          co_return;
      });

    host.start(cancellation_token).get();


    vlib::http::http_client client(pool);

    auto request = client.send("GET", "http://localhost:8000/", vlib::core::cancellation_token(cancellation_source)).get();
    request.ensureStatus(cancellation_token).get();

    answer = request.as_string(cancellation_source).get();

    request.close(cancellation_source).get();

    host.stop(vlib::core::cancellation_token(cancellation_source)).get();
    pool.stop();
  }

  GTEST_ASSERT_EQ("<html><body>Hello world</body></html>", answer);
}
*/