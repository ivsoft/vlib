/*
Copyright (c) 2017, Vadim Malyshev, lboss75@gmail.com
All rights reserved
*/
#include <gtest/gtest.h>
#include <vlib_service_provider.h>
#include "memory_leak_detector.h"

class ILogger {
public:
  virtual ~ILogger() {}
  virtual void log(std::string message) = 0;

};

class LoggerImpl : public ILogger {
private:
  std::function<void(std::string)> _callback;
public:
  LoggerImpl(std::function<void(std::string)> callback)
  : _callback(std::move(callback)) {
  }

  virtual void log(std::string message) {
    _callback(std::move(message));
  }
};

class ITest {
public:
  virtual ~ITest() {}
  virtual void test() = 0;
};

class TestImpl : public ITest {
private:
  std::shared_ptr<ILogger> _logger;
public:
  TestImpl(std::shared_ptr<ILogger> logger)
    : _logger(std::move(logger)) {}

  virtual void test() {
    _logger->log("Test");
  }
};

class IScopedTest {
public:
  virtual ~IScopedTest() {}
};

class ScopedTestImpl : public IScopedTest {
private:
public:
};

void add_logger(vlib::core::service_collection & services, std::function<void(std::string)> callback){
  services.add_singleton<ILogger>([callback](vlib::core::service_provider& sp) -> std::shared_ptr<ILogger> {
    return std::make_shared<LoggerImpl>(callback);
   });
};

void add_test(vlib::core::service_collection& services) {
  services.add_transient<ITest>([](vlib::core::service_provider& sp) -> std::shared_ptr<ITest> {
    return std::make_shared<TestImpl>(sp.get_required<ILogger>());
  });
};

void add_scoped_test(vlib::core::service_collection& services) {
  services.add_scoped<IScopedTest>([](vlib::core::service_provider& sp) -> std::shared_ptr<IScopedTest> {
    return std::make_shared<ScopedTestImpl>();
    });
};


TEST(test_core, test_service_provider) {
  memory_leak_detector leak_detector;

  std::string log;

  vlib::core::service_collection services;

  add_logger(services, [&log](std::string message) {
    log += message;
  });
  add_test(services);
    
  auto sp = services.build();

  auto test = sp->get_required<ITest>();
  test->test();

  GTEST_ASSERT_EQ("Test", log);
}

TEST(test_core, test_service_provider_lifetime) {
  memory_leak_detector leak_detector;

  std::string log;

  vlib::core::service_collection services;

  add_logger(services, [&log](std::string message) {
    log += message;
    });
  add_test(services);

  auto sp = services.build();

  auto logger1 = sp->get_required<ILogger>();
  auto logger2 = sp->get_required<ILogger>();

  auto test1 = sp->get_required<ITest>();
  auto test2 = sp->get_required<ITest>();

  GTEST_ASSERT_EQ(logger1.get(), logger2.get());
  GTEST_ASSERT_NE(test1.get(), test2.get());
}

TEST(test_core, test_service_provider_scoped) {
  memory_leak_detector leak_detector;

  std::string log;

  vlib::core::service_collection services;

  add_logger(services, [&log](std::string message) {
    log += message;
    });
  add_test(services);
  add_scoped_test(services);

  auto sp = services.build();


  auto scope1 = sp->create_scope();
  auto scope2 = sp->create_scope();


  auto logger11 = scope1->get_required<ILogger>();
  auto logger12 = scope1->get_required<ILogger>();
  auto logger21 = scope2->get_required<ILogger>();
  auto logger22 = scope2->get_required<ILogger>();

  auto test11 = scope1->get_required<ITest>();
  auto test12 = scope1->get_required<ITest>();
  auto test21 = scope2->get_required<ITest>();
  auto test22 = scope2->get_required<ITest>();

  auto scopeTest11 = scope1->get_required<IScopedTest>();
  auto scopeTest12 = scope1->get_required<IScopedTest>();
  auto scopeTest21 = scope2->get_required<IScopedTest>();
  auto scopeTest22 = scope2->get_required<IScopedTest>();

  GTEST_ASSERT_EQ(logger11.get(), logger12.get());
  GTEST_ASSERT_EQ(logger12.get(), logger21.get());
  GTEST_ASSERT_EQ(logger21.get(), logger22.get());

  GTEST_ASSERT_NE(test11.get(), test12.get());
  GTEST_ASSERT_NE(test12.get(), test21.get());
  GTEST_ASSERT_NE(test21.get(), test22.get());

  GTEST_ASSERT_EQ(scopeTest11.get(), scopeTest12.get());
  GTEST_ASSERT_NE(scopeTest11.get(), scopeTest21.get());
  GTEST_ASSERT_NE(scopeTest11.get(), scopeTest22.get());
  GTEST_ASSERT_EQ(scopeTest21.get(), scopeTest22.get());
}
