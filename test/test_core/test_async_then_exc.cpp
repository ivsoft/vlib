/*
Copyright (c) 2017, Vadim Malyshev, lboss75@gmail.com
All rights reserved
*/
#include <gtest/gtest.h>
#include <future>
#include <thread>
#include "vlib_task.h"
#include "vlib_async_enumerable.h"
#include "memory_leak_detector.h"

class exc_result
{
private:
  const int _result;
public:
  exc_result(int result) : _result(result) {}
  exc_result(const exc_result& original) : _result(original._result) {}
  exc_result(exc_result&& original) : _result(original._result) {}

  const int get() const { return _result; }
};

static vlib::core::task<int> async_add(int a, int b)
{
  auto result = std::make_shared<vlib::core::task<int>::promise_type>();
  vlib::core::task_pool::shared_task_pool->add([result, a, b]() {
    int c = a + b;
    result->set_exception(std::make_exception_ptr(exc_result(c)));
  });

  return result->get_return_object();
}


static vlib::core::task<int> async_fib(int n)
{
  if (n <= 2)
    throw exc_result(1);

  int a = 1;
  int b = 1;

  // iterate computing fib(n)
  for (int i = 0; i < n - 2; ++i)
  {
    int c;
    try
    {
      co_await async_add(a, b);
    }
    catch (exc_result ex) {
      c = ex.get();
    }
    a = b;
    b = c;
  }

  throw exc_result(b);
  co_return 0;
}

static int calc_fib(int n) {
  try
  {
    async_fib(n).get();
  }
  catch (exc_result ex) {
    return ex.get();
  }
  return -1;
}

TEST(test_async_then_exc, test_fib) {
  memory_leak_detector leak_detector;

  vlib::core::task_pool::shared_task_pool = new vlib::core::task_pool();
  vlib::core::thread_pool scheduler(2 * (size_t)std::thread::hardware_concurrency());

  std::unique_ptr<int[]> results(new int[1000]);
  for (int i = 0; i < 1000; ++i) {
    const auto pos = i;
    scheduler.add([pos, &results]() {
        results[pos] = calc_fib(6);
      });
  }
  scheduler.wait_all();

  for (int i = 0; i < 1000; ++i) {
    GTEST_ASSERT_EQ(results[i], 8) << " at " << i;//1,1,2,3,5,8
  }

  delete vlib::core::task_pool::shared_task_pool;
}

static vlib::core::async_enumerable<int> get_fib_nums() {
  int v;
  try { co_await async_fib(1); } catch (exc_result ex) { v = ex.get(); } co_yield v;
  try { co_await async_fib(2); } catch (exc_result ex) { v = ex.get(); } co_yield v;
  try { co_await async_fib(3); } catch (exc_result ex) { v = ex.get(); } co_yield v;
  try { co_await async_fib(4); } catch (exc_result ex) { v = ex.get(); } co_yield v;
  try { co_await async_fib(5); } catch (exc_result ex) { v = ex.get(); } co_yield v;
  try { co_await async_fib(6); } catch (exc_result ex) { v = ex.get(); } co_yield v;
}

static vlib::core::task<int> get_fib_sum() {
  auto gen = get_fib_nums();
  int result = 0;
  while(co_await gen.next()) {
    result += gen.value();
  }
  co_return result;
}

TEST(test_async_then_exc, test_fib_enumerable) {
  memory_leak_detector leak_detector;

  vlib::core::task_pool::shared_task_pool = new vlib::core::task_pool();

  for (int i = 0; i < 1000; ++i) {
    auto result = get_fib_sum().get();
    GTEST_ASSERT_EQ(result, 1 + 1 + 2 + 3 + 5 + 8) << " at " << i;//1,1,2,3,5,8
  }

  delete vlib::core::task_pool::shared_task_pool;
}


TEST(test_async_then_exc, test_fib_enumerable_async) {
  memory_leak_detector leak_detector;

  vlib::core::task_pool::shared_task_pool = new vlib::core::task_pool();
  vlib::core::thread_pool scheduler(2 * (size_t)std::thread::hardware_concurrency());

  std::unique_ptr<int[]> results(new int[1000]);
  for (int i = 0; i < 1000; ++i) {
    const auto pos = i;
    scheduler.add([pos, &results]() {
      results[pos] = get_fib_sum().get();
      });
  }
  scheduler.wait_all();

  for (int i = 0; i < 1000; ++i) {
    GTEST_ASSERT_EQ(results[i], 1 + 1 + 2 + 3 + 5 + 8) << " at " << i;//1,1,2,3,5,8
  }

  delete vlib::core::task_pool::shared_task_pool;
}

