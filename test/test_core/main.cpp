#include <gtest/gtest.h>

int main(int argc, char** argv) {
  std::srand(unsigned(std::time(0)));

  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}

