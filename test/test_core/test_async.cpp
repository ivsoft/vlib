/*
Copyright (c) 2017, Vadim Malyshev, lboss75@gmail.com
All rights reserved
*/
#include <gtest/gtest.h>
#include <future>
#include <thread>
#include "vlib_task.h"
#include "vlib_async_enumerable.h"
#include "memory_leak_detector.h"

static vlib::core::task<int> async_add(int a, int b)
{
  auto result = std::make_shared<vlib::core::task<int>::promise_type>();
  vlib::core::task_pool::shared_task_pool->add([result, a, b]() {
    int c = a + b;
    result->return_value(c);
  });

  return result->get_return_object();
}


static vlib::core::task<int> async_fib(int n)
{
  if (n <= 2)
    co_return 1;

  int a = 1;
  int b = 1;

  // iterate computing fib(n)
  for (int i = 0; i < n - 2; ++i)
  {
    int c = co_await async_add(a, b);
    a = b;
    b = c;
  }

  co_return b;
}

static int calc_fib(int n) {
  return async_fib(n).get();
}

TEST(test_async, test_fib) {
  memory_leak_detector leak_detector;

  vlib::core::task_pool::shared_task_pool = new vlib::core::task_pool();
  vlib::core::thread_pool scheduler(2 * (size_t)std::thread::hardware_concurrency());

  std::unique_ptr<int[]> results(new int[10000]);
  for (int i = 0; i < 10000; ++i) {
    const auto pos = i;
    scheduler.add([pos, &results]() {
        results[pos] = calc_fib(6);
      });
  }
  scheduler.wait_all();

  for (int i = 0; i < 10000; ++i) {
    GTEST_ASSERT_EQ(results[i], 8) << " at " << i;//1,1,2,3,5,8
  }

  delete vlib::core::task_pool::shared_task_pool;
}

static vlib::core::async_enumerable<int> get_fib_nums() {
  co_yield co_await async_fib(1);
  co_yield co_await async_fib(2);
  co_yield co_await async_fib(3);
  co_yield co_await async_fib(4);
  co_yield co_await async_fib(5);
  co_yield co_await async_fib(6);
}

static vlib::core::task<int> get_fib_sum() {
  auto gen = get_fib_nums();
  int result = 0;
  while(co_await gen.next()) {
    result += gen.value();
  }
  co_return result;
}

TEST(test_async, test_fib_enumerable_async) {
  memory_leak_detector leak_detector;

  vlib::core::task_pool::shared_task_pool = new vlib::core::task_pool();

  std::unique_ptr<int[]> results(new int[10000]);
  for (int i = 0; i < 10000; ++i) {
    const auto pos = i;

    get_fib_sum().then([&results, pos](vlib::core::task<int> t) {
      results[pos] = t.get();
      });
  }
  vlib::core::task_pool::shared_task_pool->wait_all();
  delete vlib::core::task_pool::shared_task_pool;


  for (int i = 0; i < 10000; ++i) {
    GTEST_ASSERT_EQ(results[i], 1 + 1 + 2 + 3 + 5 + 8) << " at " << i;
  }
}

