#include <gtest/gtest.h>
#include "vlib_string_utils.h"

TEST(test_string_utils, test_trim_left) {
  GTEST_ASSERT_EQ("", vlib::core::trim_left(""));
  GTEST_ASSERT_EQ("", vlib::core::trim_left("", " \r"));

  GTEST_ASSERT_EQ("test", vlib::core::trim_left("test"));
  GTEST_ASSERT_EQ("test", vlib::core::trim_left("test", " \r"));

  GTEST_ASSERT_EQ("te st", vlib::core::trim_left("te st"));
  GTEST_ASSERT_EQ("te st", vlib::core::trim_left("te st", " \r"));

  GTEST_ASSERT_EQ("", vlib::core::trim_left("\r "));
  GTEST_ASSERT_EQ("", vlib::core::trim_left("\r ", " \r"));

  GTEST_ASSERT_EQ("test \r\n\t", vlib::core::trim_left(" \r\n\ttest \r\n\t"));
  GTEST_ASSERT_EQ("\n\ttest \r\n\t", vlib::core::trim_left(" \r\n\ttest \r\n\t", " \r"));
}

TEST(test_string_utils, test_trim_right) {
  GTEST_ASSERT_EQ("", vlib::core::trim_right(""));
  GTEST_ASSERT_EQ("", vlib::core::trim_right("", " \r"));

  GTEST_ASSERT_EQ("test", vlib::core::trim_right("test"));
  GTEST_ASSERT_EQ("test", vlib::core::trim_right("test", " \r"));

  GTEST_ASSERT_EQ("te st", vlib::core::trim_right("te st"));
  GTEST_ASSERT_EQ("te st", vlib::core::trim_right("te st", " \r"));

  GTEST_ASSERT_EQ("", vlib::core::trim_right("\r "));
  GTEST_ASSERT_EQ("", vlib::core::trim_right("\r ", " \r"));

  GTEST_ASSERT_EQ(" \r\n\ttest", vlib::core::trim_right(" \r\n\ttest \r\n\t"));
  GTEST_ASSERT_EQ(" \r\n\ttest \r", vlib::core::trim_right(" \r\n\ttest \r\n\t", "\t\n"));
}

TEST(test_string_utils, test_trim_all) {
  GTEST_ASSERT_EQ("", vlib::core::trim(""));
  GTEST_ASSERT_EQ("", vlib::core::trim("", " \r"));

  GTEST_ASSERT_EQ("test", vlib::core::trim("test"));
  GTEST_ASSERT_EQ("test", vlib::core::trim("test", " \r"));

  GTEST_ASSERT_EQ("te st", vlib::core::trim("te st"));
  GTEST_ASSERT_EQ("te st", vlib::core::trim("te st", " \r"));

  GTEST_ASSERT_EQ("", vlib::core::trim("\r "));
  GTEST_ASSERT_EQ("", vlib::core::trim("\r ", " \r"));

  GTEST_ASSERT_EQ("test", vlib::core::trim(" \r\n\ttest \r\n\t"));
  GTEST_ASSERT_EQ("test \r", vlib::core::trim(" \n\ttest \r\n\t", " \n\t"));
}

static void sequence_equals(std::list<std::string_view> expected, std::list<std::string_view> result)
{
  GTEST_ASSERT_EQ(expected.size(), result.size());
  auto p1 = expected.begin();
  auto p2 = result.begin();
  for (int index = 0; expected.end() != p1; ++index, ++p1, ++p2) {
    GTEST_ASSERT_EQ(*p1, *p2) << " at " << index;
  }
}

TEST(test_string_utils, test_split_string) {

  sequence_equals({ "one", "two", "three" }, vlib::core::split_string("one two three"));
  sequence_equals({ "one", "two", "three" }, vlib::core::split_string("one two three", " \r"));

  sequence_equals({ "one", "two", "", "three" }, vlib::core::split_string("one two  three"));
  sequence_equals({ "one", "two", "", "three" }, vlib::core::split_string("one two \rthree", " \r"));

  sequence_equals({ "one", "two", "", "three" }, vlib::core::split_string("one two  three"));
  sequence_equals({ "one", "two", "\rthree" }, vlib::core::split_string("one two \rthree", " "));

  sequence_equals({ "", "one", "two", "", "three", "" }, vlib::core::split_string(" one two  three "));
  sequence_equals({ "", "one", "two", "\rthree", "" }, vlib::core::split_string(" one two \rthree ", " "));

  sequence_equals({ "", "", "one", "two", "", "three", "", ""}, vlib::core::split_string("  one two  three  "));
  sequence_equals({ "", "", "one", "two", "\rthree", "", "" }, vlib::core::split_string("  one two \rthree  ", " "));

  sequence_equals({ "one", "two", "three test ss " }, vlib::core::split_string("one two\rthree test ss ", 3));
  sequence_equals({ "one", "two\rthree", "test ss " }, vlib::core::split_string("one two\rthree test ss ", " ", 3));

  sequence_equals({ "", "", "one two  three  " }, vlib::core::split_string("  one two  three  ", 3));
  sequence_equals({ "", "", "one two \rthree  " }, vlib::core::split_string("  one two \rthree  ", " ", 3));
}
